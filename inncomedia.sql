-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2014 at 08:52 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inncomedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `isi` text NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `featured_image` text NOT NULL,
  `featured_image_thumb` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `summary`, `isi`, `permalink`, `featured_image`, `featured_image_thumb`, `author`, `date`, `status`) VALUES
(1, '10 Manfaat dan Keuntungan Memiliki Website Bagi Perusahaan', 'Sering muncul pertanyaan bagus, apa untungnya&nbsp;punya website? Apa sih manfaat punya website itu? Begitulah beberapa pertanyaan yang kerap saya jumpai. Nah lewat artikel ini, kami akan mencoba m...', '<p>Sering muncul pertanyaan bagus, apa untungnya&nbsp;punya website? Apa sih manfaat punya website itu? Begitulah beberapa pertanyaan yang kerap saya jumpai. Nah lewat artikel ini, kami akan mencoba menjelaskan mengenai apa saja keuntungan memiliki sebuah website bagi sebuah perusahaan</p>\r\n\r\n<p>Mengapa website itu penting? Mungkin sudah sangat banyak orang yang tahu betapa pentingnya website. Meski perusahaan Anda sudah punya brosur, sudah pasang iklan di mana-mana, sudah dikenal di masyarakat, tetapi sebenarnya masih butuh website.</p>\r\n\r\n<p>Nah inilah&nbsp;10 manfaat yang akan didapatkan perusahaan jika memiliki website.</p>\r\n\r\n<h3><strong>Sarana Penjualan Produk</strong></h3>\r\n\r\n<p>Jika Anda punya produk barang atau jasa yang ingin dipasarkan di internet, maka Anda harus punya website. Website itu fungsinya adalah sebagai alat untuk memperkenalkan produk Anda kepada dunia internet. Dengan banyaknya pemakai internet, Anda punya peluang untuk mendapatkan calon pembeli dengan jumlah yang cukup banyak juga.</p>\r\n\r\n<h3><strong>Mempermudah Komunikasi</strong></h3>\r\n\r\n<p>Lewat website, Anda bisa berkomunikasi dengan mudah. Entah itu dengan klien Anda, dengan teman-teman Anda, dengan calon konsumen dan sebagainya. Website modern itu dilengkapi dengan berbagai fitur komunikasi yang memungkinkan kita berbicara dengan pengelola website.</p>\r\n\r\n<p>Adanya kotak komentar di website, adanya form contact adalah sebagian kecil dari cara komunikasi lewat website.</p>\r\n\r\n<h3><strong>Memperkenalkan Profil Perusahaan Anda</strong></h3>\r\n\r\n<p>Semakin dikenall profil maka akan makin terkenal profil tersebut. Sebuah perusahaan atau organisasi tentu mau jika profilnya dikenal luas. Untuk itu memilih website sebagai media publikasi profil perusahaan adalah salah satu cara yang inovatif.</p>\r\n\r\n<h3><strong>Mendatangkan Calon Konsumen</strong></h3>\r\n\r\n<p>Sebuah website juga bisa dijadikan sebagai sarana untuk mendatangkan calon konsumen.&nbsp; Banyak perusahaan yang bisa menggaet klien dari luar daerah atau bahkan luar negeri lewat website yang mereka miliki. Seperti yang kita ketahui, jumlah pengunjung website tidak hanya dari Indonesia, tetapi dari luar negeri. Bahkan blog ini saja sering mendapatkan kunjungan dari luar negeri, terbukti dari banyaknya komentar asing yang masuk ke blog ini.</p>\r\n\r\n<h3><strong>Menjadi Sarana Publikasi Resmi Perusahaan Anda</strong></h3>\r\n\r\n<p>Lewat website, perusahaan Anda bisa menjadikannya sebagai sarana publikasi resmi. Misalnya perusahaan Anda mengadakan undian berhadiah, maka informasi seputar kegiatan tersebut bisa ditampilkan lewat website yang dimiliki. Bahkan, hampir semua perusahaan dituntut untuk memiliki website.</p>\r\n\r\n<h3><strong>Branding</strong></h3>\r\n\r\n<p>Membangun branding juga sangat penting bagi perusahaan. Nah peran membangun branding juga bisa dilakukan lewat website. Banyak website yang didesain secara khusus dan memiliki ciri khas tersendiri dari suatu perusahaan. Untuk itu branding lewat website juga perlu lho.</p>\r\n\r\n<h3><strong>Kemudahan Memberi Informasi</strong></h3>\r\n\r\n<p>Dengan memiliki sebuah website, perusahaan akan mudah untuk mengupdate informasi terbaru. Seperti jadwal kegiatan, berita terbaru, dan lain sebagainya. Dengan adanya website, konsumen maupun calon konsumen tetap bisa mengetahui perkembangan terbaru dari perusahaan Anda meski jaraknya sangat jauh.</p>\r\n\r\n<p>Selain itu, umumnya konsumen akan lebih suka berinteraksi dengan customer service perusahaan lewat sebuah website karena kepraktisannya.</p>\r\n\r\n<h3><strong>Kemudahan Melakukan Polling</strong></h3>\r\n\r\n<p>Polling juga merupakan sarana untuk mengetahui keadaan dan keinginan konsumen atau calon konsumen saat ini. Lewat website, kita bisa membuat polling dengan mudah dan praktis namun bisa dimaksimalkan. Di internet ada banyak penyedia layanan pembuatan form isian polling yang bisa Anda tampilkan di website Anda. Dengan kemudahan ini, tentu polling juga bisa dilakukan lewat website Anda.</p>\r\n\r\n<h3><strong>Menimbulkan Kesan Professional</strong></h3>\r\n\r\n<p>Sebuah perusahaan yang memiliki website, sudah pasti akan mendapatkan perhatian lebih dari perusahaan yang belum memiliki website. Konsumen maupun calon konsumen pasti akan melihat lebih dulu kepada perusahaan yang sudah memiliki website. Itulah sebabnya perusahaan harus punya website.</p>\r\n\r\n<h3><strong>Mencari Partner Baru</strong></h3>\r\n\r\n<p>Banyak perusahaan yang terkadang membutuhkkan partner untuk saling bekerja sama. Nah dengan mempublikasikan perusahaan Anda lewat website yang dioptimalkan, akan semakin banyak orang yang tahu mengenai keberadaan dan eksistensi perusahaan Anda. Dari sana mungkin saja perusahaan Anda bisa menemukan partner bisnis yang cocok dan menguntungkan.</p>\r\n\r\n<p>Baiklah, itulah&nbsp;10 manfaat website bagi perusahaan. Apakah perusahaan Anda sudah memiliki website sendiri? Jika belum, kami siap membuatkannya untuk Anda :)</p>\r\n\r\n<h6>sumber: http://www.blog.riauhost.net/10-manfaat-dan-keuntungan-memiliki-website-bagi-perusahaan.html</h6>', '10-manfaat-dan-keuntungan-memiliki-website-bagi-perusahaan', 'picjumbo.com_IMG_5533_.jpg', 'picjumbo.com_IMG_5533__thumb.jpg', 'Gama Unggul Priambada', '2014-12-14 16:35:56', '1'),
(2, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `desc` varchar(255) NOT NULL,
  `level` enum('1','2','3') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `image`, `desc`, `level`) VALUES
(1, 'gilapasta.png', 'Gilapasta - Restoran Pasta Yogyakarta', '2'),
(2, 'gmpro.png', 'GM Production - Sound System & EO Yogyakarta', '2'),
(3, 'hmtl.png', 'Himpunan Mahasiswa Teknik Lingkungan UII', '3'),
(4, 'kosmikgath.png', 'Gathering Komunitas Musik Informatika UII', '3'),
(5, 'majesty.png', 'Majesty Home Beauty and Salon - Denpasar, Bali', '2'),
(6, 'barokah.png', 'Barokah Tour and Travel - Yogyakarta', '3');

-- --------------------------------------------------------

--
-- Table structure for table `mailbox`
--

CREATE TABLE IF NOT EXISTS `mailbox` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `readby` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `portofolio`
--

CREATE TABLE IF NOT EXISTS `portofolio` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) NOT NULL,
  `client` varchar(255) NOT NULL,
  `works` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `featured_image` text NOT NULL,
  `featured_image_thumb` text NOT NULL,
  `company_logo` text NOT NULL,
  `company_profile` text NOT NULL,
  `project_desc` text NOT NULL,
  `youtube_video` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `portofolio`
--

INSERT INTO `portofolio` (`id`, `project_name`, `client`, `works`, `permalink`, `featured_image`, `featured_image_thumb`, `company_logo`, `company_profile`, `project_desc`, `youtube_video`, `status`) VALUES
(5, 'IM Admin Multifunctional Dashboard', 'Inncomedia', 'Dashboard Template', 'im-admin-multifunction-dashboard', 'imadmin.jpg', 'imadmin_thumb.jpg', 'logo+text_thumb.jpg', '<p>INNCOMEDIA (Innovative.Creative.Outstanding). Sesuai dengan namanya, Inncomedia adalah perusahaan yang bergerak dibidang jasa pengembangan Teknologi Informasi yang mengedepankan Inovasi, Kreatifitas dan Terkemuka yang berada di Yogyakarta. Kami menawarkan berbagai macam jasa dan produk mulai dari pembuatan website, pembuatan software, pembuatan aplikasi desktop maupun android, multimedia, IT support &amp; consultant, infrastruktur jaringan, pengadaan alat, dan hosting. Memiliki beberapa staff ahli dibidangnya masing-masing. Selain itu, kami juga memiliki banyak pengalaman dalam membuat dan mengkonsep jasa dan produk yang ditawarkan.</p>', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>\r\n\r\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.</p>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.</p>\r\n\r\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy. Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.</p>\r\n\r\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>\r\n\r\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.</p>\r\n\r\n<p>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis.</p>\r\n\r\n<p>Pellentesque porttitor, velit lacinia egestas auctor, diam eros tempus arcu, nec vulputate augue magna vel risus. Cras non magna vel ante adipiscing rhoncus. Vivamus a mi. Morbi neque. Aliquam erat volutpat. Integer ultrices lobortis eros.</p>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin semper, ante vitae sollicitudin posuere, metus quam iaculis nibh, vitae scelerisque nunc massa eget pede. Sed velit urna, interdum vel, ultricies vel, faucibus at, quam.</p>', '', '1'),
(6, 'KeyMan', '&#8212;', 'Key-In Manager', 'keyman', '12.jpg', '12_thumb.jpg', 'logo-hover_thumb.png', '<p>KeyMan (Key-In Manager) adalah sebuah web-application yang dibuat untuk membantu mahasiswa Teknik Informatika Universitas Islam Indonesia (UII) dalam hal menyusun jadwal kuliah yang nantinya akan dimasukkan ke dalam KRS mahasiswa tersebut.</p>\r\n\r\n<p>KeyMan hadir karena mahasiswa merasa kesulitan dalam mengatur jadwal kuliahnya, apalagi ditambah dengan daftar mata kuliah yang disediakan jurusan tidak diatur dalam bentuk yang enak dilihat.</p>', '<p>KeyMan dapat digunakan secara gratis oleh seluruh mahasiswa Teknik Informatika UII. Mahasiswa hanya cukup memasukkan nomor induk mahasiswa mereka (NIM) dan jumlah SKS yang akan diambil untuk dapat menggunakan KeyMan.</p>\r\n\r\n<p>Untuk menggunakan KeyMan, mahasiswa tak perlu lagi mencoret-coret kertas untuk menyusun jadwal. Mahasiswa cukup memilih jadwal yang akan dipilih cukup dengan klik-klik saja.</p>\r\n\r\n<p>Fitur-fitur yang ditawarkan KeyMan antara lain:</p>\r\n\r\n<ul>\r\n <li>Jam dan nama dosen yang dikelompokkan berdasarkan mata kuliah.</li>\r\n <li>Mengatur mata kuliah mana saja yang bisa ditampilkan di dashboard.</li>\r\n <li>Pemberitahuan jika mata kuliah bertabrakan dengan mata kuliah yang lain.</li>\r\n <li>Jadwal yang sudah dibuat dapat disimpan dalam bentuk image.</li>\r\n <li>Jadwal yang dibuat bisa disimpan dan dibuka lagi sewaktu-waktu.</li>\r\n <li>Share jadwal buatanmu dengan sekali klik.</li>\r\n <li>Buat jadwal sebanyak-banyaknya untuk membuat alternatif jadwal pada saat key-in.</li>\r\n</ul>\r\n\r\n<p>Sejak pertama kali KeyMan diluncurkan, Keyman sudah membantu mahasiswa dalam menyusun jadwal kuliahnya. Untuk saat ini, KeyMan sedang dalam pengembangan untuk menambah fitur dan kemudahan bagi mahasiswa, bukan hanya untuk Teknik Informatika namun juga untuk jurusan yang lain.</p>', '', '1'),
(7, 'Gila Pasta Website Development', 'Gila Pasta', 'Website Development', 'gila-pasta-website-development', 'landing.jpg', 'landing_thumb.jpg', 'gilapasta1_thumb.png', '<p>Gila Pasta merupakan salah satu restoran favorit anak muda di Yogyakarta yang menyediakan berbagai hidangan lezat khas Italia seperti Pizza dan Pasta. Selain hidangan yang menjadi andalan Gila Pasta, tempat yang nyaman dengan desain interior yang cantik dan khas mengusung konsep vintage menjadikan Gila Pasta tempat favorit untuk kaula muda berkumpul dan berfoto dengan pasangan dan teman-teman. Gila Pasta juga menyediakan tempat untuk Meeting, Birthday Party, Exhibiton, dan Music Session yang membuat restoran ini menjadi restoran andalan untuk berbagai acara.</p>', '<p>Restoran Gila Pasta yang sangat memperhatikan detail agar terlihat cantik hingga hal terkecil ini memerlukan website sebagai media promosi dan pengumuman yang sangat memperhatikan detail agar terlihat indah, karena itu INNCOMEDIA menghadirkan website Gila Pasta dengan fitur-fitur menarik dan cantik.</p>\r\n\r\n<ul>\r\n <li>Desain tampilan &ldquo;Single Page Design&rdquo;&nbsp; yang modern dengan kemudahan akses bagi pengunjung website agar tidak perlu menunggu untuk berpindah halaman.</li>\r\n <li>Konsep elegan dengan memadukan &ldquo;flat design&rdquo;,&ldquo;blur effect photos&rdquo;, dan warna merah yang menggoda menjadikan website ini memiliki kesan tersendiri bagi pengunjung website Gila Pasta.</li>\r\n <li>&ldquo;Twitter Comment&rdquo; menghadirkan pengalaman melihat komentar dan berbagai aktifitas dari update status twitter pengunjung&nbsp; menjadikan berkomentar lebih asik di Gila Pasta.</li>\r\n <li>&ldquo;Buku Menu&rdquo; yang unik dan menarik serta gambar makanan resolusi tinggi semakin membuat pengunjung tidak sabar untuk segera datang ke Gila Pasta dan mencoba berbagai menu andalan Gila Pasta.</li>\r\n <li>&ldquo;Find Us&rdquo; yang sangat interaktif dan mudah digunakan membuat Gila Pasta lebih mudah untuk ditemukan dari lokasi pengunjung website. Sangat membantu bagi pengunjung yang baru pertama kali ingin mencoba kelezatan Gila Pasta.</li>\r\n <li>&ldquo;Events&rdquo; yang memberikan pengunjung informasi menarik terkait acara-acara yang akan dan sedang berlangsung di Gila Pasta. Pengunjung tidak perlu ragu lagi datang ke Gila Pasta untuk menghadiri berbagai acara yang diadakan di Gila Pasta.</li>\r\n <li>&ldquo;Guest book&rdquo; memberikan kemudahan bagi pengunjung untuk memberikan saran, reservasi, dan melakukan kontak langsung dengan customer service dengan pelayanan full-hours 24 jam.</li>\r\n</ul>', '', '1'),
(8, 'Majesty Home Beauty Website Development', 'Majesty Home Beauty and Salon', 'Website Development', 'majesty-home-beauty-website-development', 'landing1.jpg', 'landing1_thumb.jpg', 'majesty1_thumb.png', '<p>Majesty Home Beauty adalah rumah kecantikan ternama yang terletak di Bali. Dengan mengkombinasikan konsep modern dan vintage yang semakin memanjakan siapa saja yang datang ke Majesty. Majesty Home Beauty menyajikan beberapa pelayanan seperti Hair and Nail Salon, Bridal, Caf&eacute;, Fashion Club, Wedding Organizer, dan Event Organizer.</p>\r\n\r\n<p>Untuk menambah nilai di bidang pemasaran maka Majesty Home Beauty bekerja sama dengan Inncomedia untuk membangun sebuah website untuk memperluas jangkauan bisnis ke berbagai tempat.</p>', '<p>Dengan konsep website modern yang sedang meroket yaitu &ldquo;Single Page Website&rdquo; yang hanya membutuhkan satu halaman saja sudah mencakup semua menu yang disajikan. Dimana tiap menu memiliki efek animasi yang berbeda-beda menambah kesan modern dan mewah bagi siapapun yang mengunjunginya.</p>\r\n\r\n<p>Pada menu pertama yaitu &ldquo;Hair &amp; Salon&rdquo; terdapat daftar menu dan harga perawatan salon Majesty. Kemudian menu Bridal terdapat 4 foto gaun yang cantik dimana ketika di &ldquo;hover&rdquo; maka foto tersebut akan melebar dan memunculkan prolog yang menjelaskan gaun itu serta terdapat daftar harga gaun. Tak hanya fokus pada bidang kecantikan, website ini juga terdapat menu &ldquo;Caf&eacute;&rdquo; yang berisi daftar makanan dan minuman yang sangat menggoda lidah.</p>\r\n\r\n<p>Menu yang tak kalah uniknya yaitu Fashion Club yang terdapat 4 kegiatan didalamnya, mereka adalah Modelling, Dance, Artist Management, dan Kids Club. Setelah menu Fashion Club, terdapat menu Wedding Organizer dan Event organizer yang berisi gambar serta penjelasan dari gambar-gambar tersebut.</p>\r\n\r\n<p>Kemudian ada 3 menu tambahan yaitu &ldquo;Gallery&rdquo; yang menampilkan foto2 dari Majesty Home Beauty, &ldquo;Contact&rdquo; menampilkan form untuk diisi pengunjung yang ingin memberi saran, kritik, dan sebagainya, dan menu yang terakhir adalah &ldquo;Map&rdquo;, menampilkan lokasi Majesty Home Beuty agar memudahkan pengunjung yang ingin mampir ke rumah kecantikan terbaik.</p>', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `portofolio_gallery`
--

CREATE TABLE IF NOT EXISTS `portofolio_gallery` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `thumb` text NOT NULL,
  `portofolio_id` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `portofolio_gallery`
--

INSERT INTO `portofolio_gallery` (`id`, `image`, `thumb`, `portofolio_id`) VALUES
(7, '1.jpg', '1_thumb.jpg', 5),
(8, '2.jpg', '2_thumb.jpg', 5),
(9, '3.jpg', '3_thumb.jpg', 5),
(10, '4.jpg', '4_thumb.jpg', 5),
(11, '5.jpg', '5_thumb.jpg', 5),
(12, '6.jpg', '6_thumb.jpg', 5),
(13, '21.jpg', '21_thumb.jpg', 6),
(14, '31.jpg', '31_thumb.jpg', 6),
(15, '41.jpg', '41_thumb.jpg', 6),
(16, '51.jpg', '51_thumb.jpg', 6),
(17, '61.jpg', '61_thumb.jpg', 6),
(18, '7.jpg', '7_thumb.jpg', 6),
(19, '8.jpg', '8_thumb.jpg', 6),
(20, '9.jpg', '9_thumb.jpg', 6),
(21, 'about_us.PNG', 'about_us_thumb.PNG', 7),
(22, 'event.PNG', 'event_thumb.PNG', 7),
(23, 'guestbook.PNG', 'guestbook_thumb.PNG', 7),
(24, 'home.PNG', 'home_thumb.PNG', 7),
(25, 'location.PNG', 'location_thumb.PNG', 7),
(26, 'menu.PNG', 'menu_thumb.PNG', 7),
(27, 'menubook.PNG', 'menubook_thumb.PNG', 7),
(28, 'bridal.png', 'bridal_thumb.png', 8),
(29, 'cafe.png', 'cafe_thumb.png', 8),
(30, 'contact.png', 'contact_thumb.png', 8),
(31, 'eo.png', 'eo_thumb.png', 8),
(32, 'fashionclub.png', 'fashionclub_thumb.png', 8),
(33, 'gallery.png', 'gallery_thumb.png', 8),
(34, 'homepage.png', 'homepage_thumb.png', 8),
(35, 'loading.png', 'loading_thumb.png', 8),
(36, 'map.png', 'map_thumb.png', 8),
(37, 'salon.png', 'salon_thumb.png', 8);

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE IF NOT EXISTS `testimoni` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `testimoni` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id`, `name`, `position`, `testimoni`, `status`) VALUES
(1, 'Dwiarty Wardhani', 'Owner, Majesy Home Beauty', 'Saya suka dengan kekreatifitasan Inncomedia. Desain yang dibuat untuk kami (Majesty) sangatlah sesuai dengan yang saya inginkan. Saya sangat senang bekerja sama dengan tim Inncomedia', '1'),
(2, 'Bismo Ambar Polah', 'Owner, Gila Pasta', 'Hasil karyanya sangat mengejutkan. Kombinasi yang diberikan sangatlah istimewa. Website Gila Pasta terlihat sangat fresh dan segar', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(30) NOT NULL,
  `role` int(2) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `display_name`, `role`, `last_login`) VALUES
(1, 'superadmin', 'f63c4488d250685a941ffcff5c8bc7af', 'Super Admin', 1, '2014-12-20 11:43:15'),
(2, 'gamaup', '6437815c5d8344b9c28798f0fff92faa', 'Gama Unggul Priambada', 7, '2014-12-18 18:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  `access_level` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `access_level`) VALUES
(1, 'Super Admin', 0),
(2, 'Manager', 1),
(3, 'Secretary', 2),
(4, 'Marketing Manager', 2),
(5, 'Technical Manager', 2),
(6, 'Marketing', 3),
(7, 'Front-End Supervisor', 3),
(8, 'Front-End Developer', 3),
(9, 'Back-End Supervisor', 3),
(10, 'Back-End Developer', 3),
(11, 'Multimedia Supervisor', 3),
(12, 'Networking Supervisor', 3),
(13, 'Networking', 3),
(14, 'Mobile App Supervisor', 3),
(15, 'Mobile App Developer', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
