var lebar = 0;

$(window).on('load resize', function() {
    lebar = $(window).width();
    
});

$(window).on('scroll', function() {
    if (lebar > 990) {
        if($(window).scrollTop() > 400) {
            $('body > nav').css({
                height: 61
            });
            $('.nav-logo').css({
                height: 40
            });
            $('.nav-group').css({
                padding: '5px 0'
            });
        } else {
            $('body > nav').css({
                height: 81
            });
            $('.nav-logo').css({
                height: 60
            });
            $('.nav-group').css({
                padding: '15px 0'
            });
        }
    }
    if($(window).scrollTop() > 400) {
        $('.backtotop').addClass('show');
    } else {
        $('.backtotop').removeClass('show');
    }
});

$(document).ready(function() {
    $('.push-nav-toggle').click(function() {
        $(this).toggleClass('pushed');
        $('main, nav, .push-nav').toggleClass('pushed');
    });
    
    //scroll to top
    $('.backtotop').click(function() {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });
    
    //whatwedo slide
    var whatwedoSlide = $("#whatwedo-slide");
    whatwedoSlide.owlCarousel({
        items: 4,
        pagination: false,
        scrollPerPage: true,
        slideSpeed: 800,
        itemsDesktopSmall: [1200,3],
        itemsTablet: [980,2],
        itemsMobile: [480,1],
        autoPlay: true
    });
    var testimoniSlide = $("#testimoni-slide");
    testimoniSlide.owlCarousel({
        singleItem: true,
        scrollPerPage: true,
        slideSpeed: 800,
        autoHeight: true,
        autoPlay: true
    });
    $('.whatwedo-prev').click(function() {
        whatwedoSlide.trigger('owl.prev');
    });
    $('.whatwedo-next').click(function() {
        whatwedoSlide.trigger('owl.next');
    });
    
     //product nav slider
    var productNavSlider = $("#product-nav-body");
    productNavSlider.owlCarousel({
        items: 5,
        pagination: false,
        scrollPerPage: false,
        slideSpeed: 800,
        itemsDesktopSmall: [1200,4],
        itemsTablet: [980,3],
        itemsMobile: [480,2],
    });
    //product nav button
    $('.product-nav-button').click(function() {
        $('.product-nav-wrapper').toggleClass('product-nav-wrapper-close');
        $(this).toggleClass('product-nav-button-down');
    });
    //project snapshot slide
    var projectSnapshot = $("#project-snapshot");
    projectSnapshot.owlCarousel({
        items: 4,
        pagination: true,
        scrollPerPage: true,
        slideSpeed: 800,
        itemsDesktopSmall: [1200,3],
        itemsTablet: [980,2],
        itemsMobile: [480,1],
    });
    $( '.swipebox' ).swipebox();

    $('#menu1').click(function() {
    $('#isi1').css('display','block');
    $('#isi2, #isi3, #isi4, #isi5').css('display', 'none');
    $('#menu1').css('color','#454753').css('text-decoration', 'underline');
    $('#menu2, #menu3, #menu4, #menu5').css('color','#bdc3c7').css('text-decoration', 'none');
});

$('#menu2').click(function() {
    $('#isi2').css('display','block');
    $('#isi1, #isi3, #isi4, #isi5').css('display', 'none');
    $('#menu2').css('color','#454753').css('text-decoration', 'underline');
    $('#menu1, #menu3, #menu4, #menu5').css('color','#bdc3c7').css('text-decoration', 'none');

});

$('#menu3').click(function() {
    $('#isi3').css('display','block');
    $('#isi2, #isi1, #isi4, #isi5').css('display', 'none');
    $('#menu3').css('color','#454753').css('text-decoration', 'underline');
    $('#menu2, #menu1, #menu4, #menu5').css('color','#bdc3c7').css('text-decoration', 'none');

});

$('#menu4').click(function() {
    $('#isi4').css('display','block');
    $('#isi2, #isi3, #isi1, #isi5').css('display', 'none');
    $('#menu4').css('color','#454753').css('text-decoration', 'underline');
    $('#menu2, #menu3, #menu1, #menu5').css('color','#bdc3c7').css('text-decoration', 'none');

});

$('#menu5').click(function() {
    $('#isi5').css('display','block');
    $('#isi2, #isi3, #isi4, #isi1').css('display', 'none');
    $('#menu5').css('color','#454753').css('text-decoration', 'underline');
    $('#menu2, #menu3, #menu4, #menu1').css('color','#bdc3c7').css('text-decoration', 'none');

});

(function() {
    
    var thumbsSpacing = 15;

    $('.filter').css('margin-bottom', thumbsSpacing + 'px');
    $('.thumbnail').addClass('showThumb').addClass('fancybox').attr('rel', 'group');

    $('a.sortLink').on('click', function(e) {
        e.preventDefault();
        $('a.sortLink').removeClass('selected');
        $(this).addClass('selected');

        var category = $(this).data('category');
        filterThumbs(category);
    });

    positionThumbs();
    setInterval(checkViewport, 750);

    function checkViewport() {

        var photosWidth = $('.photos').width(),
            thumbsContainerWidth = $('.thumbnail_wrap').width(),
            thumbnailWidth = $('a.thumbnail:first-child').outerWidth();

        if ( photosWidth < thumbsContainerWidth ) {
            positionThumbs();
        }

        if ( (photosWidth - thumbnailWidth) > thumbsContainerWidth ) {
            positionThumbs();
        }
    }

    function filterThumbs(category) {
        
        $('a.thumbnail').each(function() {
            var thumbCategory = $(this).data('categories');

            if ( category === 'all' ) {
                $(this).addClass('showThumb').removeClass('hideThumb').attr('rel', 'group');
            } else {
                if ( thumbCategory.indexOf(category) !== -1 ) {
                    $(this).addClass('showThumb').removeClass('hideThumb').attr('rel', 'group');
                } else {
                    $(this).addClass('hideThumb').removeClass('showThumb').attr('rel', 'none');
                }
            }
        });

        positionThumbs();

    }

    function positionThumbs() {

        $('a.thumbnail.hideThumb').animate({
            'opacity': 0
        }, 500, function() {
            $(this).css({
                'display': 'none',
                'top': '0px',
                'left': '0px'
            });
        });

        var containerWidth = $('.photos').width(),
            thumbRow = 0,
            thumbColumn = 0,
            thumbWidth = $('.thumbnail img:first-child').outerWidth() + thumbsSpacing,
            thumbHeight = $('.thumbnail img:first-child').outerHeight() + thumbsSpacing,
            maxColumns = Math.floor( containerWidth / thumbWidth );

        $('a.thumbnail.showThumb').each(function(index){
            var remainder = ( index%maxColumns ) / 100,
                maxIndex = 0;

            if( remainder === 0 ) {
                if( index !== 0 ) {
                    thumbRow += thumbHeight;
                }
                thumbColumn = 0;
            } else {
                thumbColumn += thumbWidth;
            }

            $(this).css('display', 'block').animate({
                'opacity': 1,
                'top': thumbRow + 'px',
                'left': thumbColumn + 'px'
            }, 500);

            var newWidth = thumbColumn + thumbWidth,
                newHeight = thumbRow + thumbHeight;
            $('.thumbnail_wrap').css({
                // 'width': newWidth + 'px',
                'height': newHeight + 'px'
            });
        });

        findFancyBoxLinks();
    }

    function findFancyBoxLinks() {

        $('a.fancybox[rel="group"]').fancybox({
            'transitionIn' : 'elastic',
            'transitionOut' : 'elastic',
            'titlePosition' : 'over',
            'speedIn' : 500,
            'overlayColor' : '#000',
            'padding' : 0,
            'overlayOpacity' : .75
        });
    }

})();

});