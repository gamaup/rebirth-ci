<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('user_model', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
        	$data = array(
            	'pagetitle' => 'Dashboard ~ IMADMIN',
            	'pos_parent' => 'dashboard',
            	'pos_child' => '',
            	'title' => 'Dashboard',
            	'subtitle' => '',
            	'action' => "<a class='button button-blue' href='".base_url()."imadmin/manage_user/add'><i class='fa fa-thumb-tack'></i> Add User</a>",
            	'breadcrumb' => array(),
            	'content' => 'imadmin/content/dashboard'
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

}

?>
