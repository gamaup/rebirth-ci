<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mailbox extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('user_model', '', TRUE);
        $this->load->model('mailbox_model', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
        	$data = array(
            	'pagetitle' => 'Mailbox ~ IMADMIN',
            	'pos_parent' => 'mailbox',
            	'pos_child' => 'allmessages',
            	'title' => 'Mailbox',
            	'subtitle' => 'All Messages',
            	'action' => '',
            	'breadcrumb' => array('Mailbox'),
            	'content' => 'imadmin/content/mailbox',
            	'data_mail' => $this->mailbox_model->get_all_mail(),
            	'data_con' => $this
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function mail($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$data = array(
            	'pagetitle' => 'Mailbox ~ IMADMIN',
            	'pos_parent' => 'mailbox',
            	'pos_child' => '',
            	'title' => 'Mailbox',
            	'subtitle' => 'View Message',
            	'action' => '',
            	'breadcrumb' => array('<a href="'.base_url().'imadmin/mailbox">Mailbox</a>', 'View Message'),
            	'content' => 'imadmin/content/mailbox_view',
            	'data_mail' => $this->mailbox_model->get_mail_by_id($id)
            );
            $this->load->view('imadmin/template/page', $data);
            $this->read_mail($id);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function delete($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->mailbox_model->delete($id);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    function check_read($id = '') {
        $data_login = $this->session->userdata('logged_in');
    	$user = explode(" ",$this->mailbox_model->get_readby_by_id($id));
    	$current_user = $data_login['username'];
    	if(!in_array($current_user, $user)) {
    		echo "class='unread'";
    	}
    }

    private function read_mail($id = '') {
        $data_login = $this->session->userdata('logged_in');
    	$user = explode(" ",$this->mailbox_model->get_readby_by_id($id));
    	$current_user = $data_login['username'];
    	if(!in_array($current_user, $user)) {
    		array_push($user, $current_user);
    		$new_readby = implode(" ", $user);
    		$data = array('readby' => $new_readby);
    		$this->mailbox_model->update_readby($id, $data);
    	}
    }
}

?>
