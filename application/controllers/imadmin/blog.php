<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('blog_model', '', TRUE);
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
            $this->blog_model->clean_blog();
            $data = array(
                'pagetitle' => 'Blog ~ IMADMIN',
                'pos_parent' => 'blog',
                'pos_child' => 'postlist',
                'title' => 'Blog',
                'subtitle' => 'List of Blog Post',
                'action' => "<a class='button button-blue' href='".base_url()."imadmin/blog/add'><i class='fa fa-plus'></i> New Post</a>",
                'breadcrumb' => array('Blog'),
                'content' => 'imadmin/content/blog',
                'data_blog' => $this->blog_model->get_all_blog()
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            $this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function add() {
    	if ($this->session->userdata('logged_in')) {
	    	$id = $this->blog_model->create_new();
	    	redirect('imadmin/blog/post/'.$id);
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function cancel($id = '') {
        if ($this->session->userdata('logged_in')) {
            $this->blog_model->delete($id);
            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Post was deleted.<i class='fa fa-times'></i></p>
            </div>");
            redirect('imadmin/blog');
        } else {
            $this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function post($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_message('is_unique', 'Permalink already exist.');
	        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[blog.permalink]');
	        $this->form_validation->set_rules('isi', 'Isi', 'trim|required|xss_clean');
            $data_login = $this->session->userdata('logged_in');

	        if ($this->form_validation->run() == FALSE) {
		    	$data = array(
	            	'pagetitle' => 'Blog ~ IMADMIN',
	            	'pos_parent' => 'blog',
	            	'pos_child' => 'new_post',
	            	'title' => 'Blog',
	            	'subtitle' => 'Create New Post',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/blog/cancel/".$id."'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/blog">Blog</a>', 'New Post'),
	            	'content' => 'imadmin/content/blog_new',
	            	'data_blog' => $this->blog_model->get_blog_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
            } else {
	        	if (!$this->upload->do_upload("featured_image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $this->create_thumb($this->upload->upload_path.$this->upload->file_name);
                    $featured_image = $data['upload_data']['file_name'];
                    $featured_image_thumb = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];
                }
                if ($_FILES['featured_image']['size'] == 0) {
	                $data = array(
	                	'title' => $this->input->post('title'),
	                	'date' => date('Y-m-d H:i:s'),
	                	'isi' => $this->input->post('isi'),
	                	'summary' => strip_tags(substr($this->input->post('isi'),0,200)).'...',
	                	'permalink' => $this->input->post('permalink'),
	                	'author' => $data_login['display_name'],
	                	'status' => '1',
	                );
	            } else {
                    $data = array(
	                	'title' => $this->input->post('title'),
	                	'date' => date('Y-m-d H:i:s'),
	                	'isi' => $this->input->post('isi'),
	                	'summary' => strip_tags(substr($this->input->post('isi'),0,200)).'...',
	                	'permalink' => $this->input->post('permalink'),
	                	'author' => $data_login['display_name'],
	                	'status' => '1',
	                	'featured_image' => $featured_image,
	                	'featured_image_thumb' => $featured_image_thumb
	                );
	            }
	        	$this->blog_model->update_blog($data, $id);
	            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Success!</b> Post '".$this->input->post('title')."' was created.<i class='fa fa-times'></i></p>
	            </div>");
	        	redirect('imadmin/blog', 'refresh');
	        }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function edit($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('isi', 'Isi', 'trim|required|xss_clean');
            $data_login = $this->session->userdata('logged_in');

	        if ($this->form_validation->run() == FALSE) {
		    	$data = array(
	            	'pagetitle' => 'Blog ~ IMADMIN',
	            	'pos_parent' => 'blog',
	            	'pos_child' => 'new_post',
	            	'title' => 'Blog',
	            	'subtitle' => 'Edit Post',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/blog/'><i class='fa fa-times'></i> Close</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/blog">Blog</a>', 'New Post'),
	            	'content' => 'imadmin/content/blog_edit',
	            	'data_blog' => $this->blog_model->get_blog_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
            } else {
	        	if (!$this->upload->do_upload("featured_image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $this->create_thumb($this->upload->upload_path.$this->upload->file_name);
                    $featured_image = $data['upload_data']['file_name'];
                    $featured_image_thumb = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];
                }
                if ($_FILES['featured_image']['size'] == 0) {
	                $data = array(
	                	'title' => $this->input->post('title'),
	                	'date' => date('Y-m-d H:i:s'),
	                	'isi' => $this->input->post('isi'),
	                	'summary' => strip_tags(substr($this->input->post('isi'),0,200)).'...',
	                	'permalink' => $this->input->post('permalink'),
	                	'author' => $data_login['display_name'],
	                	'status' => '1',
	                );
	            } else {
	            	$delete1 = $this->input->post('image_delete');
	            	$delete2 = $this->input->post('image_delete2');
                    unlink('assets/uploads/'.$delete1);
                    unlink('assets/uploads/'.$delete2);
                    //upload foto
                    $data = array(
	                	'title' => $this->input->post('title'),
	                	'date' => date('Y-m-d H:i:s'),
	                	'isi' => $this->input->post('isi'),
	                	'summary' => strip_tags(substr($this->input->post('isi'),0,200)).'...',
	                	'permalink' => $this->input->post('permalink'),
	                	'author' => $data_login['display_name'],
	                	'status' => '1',
	                	'featured_image' => $featured_image,
	                	'featured_image_thumb' => $featured_image_thumb
	                );
	            }
	        	$this->blog_model->update_blog($data, $id);
	            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Success!</b> Post '".$this->input->post('title')."' was edited.<i class='fa fa-times'></i></p>
	            </div>");
	        	redirect('imadmin/blog', 'refresh');
	        }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    private function create_thumb($file) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['new_image'] = './assets/uploads';
        $config['maintain_ratio'] = TRUE;
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '_thumb';
        $config['width'] = 400;
        $config['height'] = 300;
        $config['master_dim'] = 'auto';
        $config['maintain_ratio'] = TRUE;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    
}

?>
