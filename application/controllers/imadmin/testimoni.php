<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testimoni extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('testimoni_model', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
        	$data = array(
            	'pagetitle' => 'Testimonial ~ IMADMIN',
            	'pos_parent' => 'testimoni',
            	'pos_child' => '',
            	'title' => 'Testimoni',
            	'subtitle' => 'Bacotan Orang Yang Udah Puas',
            	'action' => "<a class='button button-blue' href='".base_url()."imadmin/testimoni/add'><i class='fa fa-plus'></i> New Testimonial</a>",
            	'breadcrumb' => array('Testimoni'),
            	'content' => 'imadmin/content/testimoni',
            	'data_testimoni' => $this->testimoni_model->get_all_testimoni()
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('logged_in')) {
    		$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[30]|alpha-numeric');
	        $this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('testimoni', 'Testimoni', 'trim|required|xss_clean');

	        if ($this->form_validation->run() == FALSE) {
	            $data = array(
	            	'pagetitle' => 'Testimoni ~ IMADMIN',
	            	'pos_parent' => 'testimoni',
	            	'pos_child' => '',
	            	'title' => 'Tesmoni',
	            	'subtitle' => 'Add New Testimoni',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/testimoni'><i class='fa fa-times'></i> Close</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/testimoni/">Testimoni</a>', 'Add Testimoni'),
	            	'content' => 'imadmin/content/testimoni_add'
	            );
	            $this->load->view('imadmin/template/page', $data);
	        } else {
	        	$data = array(
	        		'name' => $this->input->post('name'),
					'position' => $this->input->post('position'),
					'testimoni' => $this->input->post('testimoni'),
					'status' => '1'
				);
	        	$this->testimoni_model->add_testimoni($data);
	        	$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Testimoni successfully added to the database.<i class='fa fa-times'></i></p>
            </div>");
	        	redirect('imadmin/testimoni', 'refresh');
	        }
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function edit($id = '') {
        if ($this->session->userdata('logged_in')) {
    		$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[30]|alpha-numeric');
	        $this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('testimoni', 'Testimoni', 'trim|required|xss_clean');

	        if ($this->form_validation->run() == FALSE) {
	            $data = array(
	            	'pagetitle' => 'Testimoni ~ IMADMIN',
	            	'pos_parent' => 'testimoni',
	            	'pos_child' => '',
	            	'title' => 'Tesmoni',
	            	'subtitle' => 'Edit Testimoni',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/testimoni'><i class='fa fa-times'></i> Close</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/testimoni/">Testimoni</a>', 'Edit Testimoni'),
	            	'content' => 'imadmin/content/testimoni_edit',
	            	'data_testimoni' => $this->testimoni_model->get_testimoni_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
	        } else {
	        	$data = array(
	        		'name' => $this->input->post('name'),
					'position' => $this->input->post('position'),
					'testimoni' => $this->input->post('testimoni'),
					'status' => '1'
				);
	        	$this->testimoni_model->update_testimoni($data, $id);
	        	$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Testimoni successfully updated.<i class='fa fa-times'></i></p>
            </div>");
	        	redirect('imadmin/testimoni', 'refresh');
	        }
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function delete($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->testimoni_model->delete($id);
	        	$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Testimoni successfully deleted.<i class='fa fa-times'></i></p>
            </div>");
	        	redirect('imadmin/testimoni', 'refresh');
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }
}

?>
