<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('clients_model', '', TRUE);
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
        	$data = array(
            	'pagetitle' => 'Clients ~ IMADMIN',
            	'pos_parent' => 'clients',
            	'pos_child' => '',
            	'title' => 'Clients',
            	'subtitle' => 'Mereka Yang Telah Dipuaskan Kita',
            	'action' => "<a class='button button-blue' href='".base_url()."imadmin/clients/add'><i class='fa fa-plus'></i> New Client</a>",
            	'breadcrumb' => array('Clients'),
            	'content' => 'imadmin/content/clients',
            	'data_clients' => $this->clients_model->get_all_clients()
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('logged_in')) {
    		$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('desc', 'Client Name', 'trim|required|xss_clean|max_length[50]|alpha-numeric');

	        if ($this->form_validation->run() == FALSE) {
	            $data = array(
	            	'pagetitle' => 'Clients ~ IMADMIN',
	            	'pos_parent' => 'clients',
	            	'pos_child' => '',
	            	'title' => 'Clients',
	            	'subtitle' => 'Add New Client',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/clients'><i class='fa fa-times'></i> Close</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/clients/">Clients</a>', 'Add Client'),
	            	'content' => 'imadmin/content/clients_add'
	            );
	            $this->load->view('imadmin/template/page', $data);
	        } else {
	        	if (!$this->upload->do_upload("image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $image = $data['upload_data']['file_name'];
                }
	        	$data = array(
	        		'desc' => $this->input->post('desc'),
                    'level' => $this->input->post('level'),
					'image' => $image
				);
	        	$this->clients_model->add($data);
	        	$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Client successfully added to the database.<i class='fa fa-times'></i></p>
            </div>");
	        	redirect('imadmin/clients', 'refresh');
	        }
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function delete($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->clients_model->delete($id);
	        	$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Testimoni successfully deleted.<i class='fa fa-times'></i></p>
            </div>");
	        	redirect('imadmin/clients', 'refresh');
        } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }
}

?>
