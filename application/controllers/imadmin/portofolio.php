<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Portofolio extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('portofolio_model', '', TRUE);
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
            $this->portofolio_model->clean_portofolio();
            $data = array(
                'pagetitle' => 'Portofolio ~ IMADMIN',
                'pos_parent' => 'portofolio',
                'pos_child' => 'portofoliolist',
                'title' => 'Portofolio',
                'subtitle' => 'List of Portofolio',
                'action' => "<a class='button button-blue' href='".base_url()."imadmin/portofolio/add'><i class='fa fa-plus'></i> Create New Portofolio</a>",
                'breadcrumb' => array('Portofolio'),
                'content' => 'imadmin/content/portofolio',
                'data_portofolio' => $this->portofolio_model->get_all_portofolio()
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            $this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function add() {
    	if ($this->session->userdata('logged_in')) {
	    	$id = $this->portofolio_model->create_new();
	    	redirect('imadmin/new-portofolio/'.$id.'/step1');
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function cancel($id = '') {
        if ($this->session->userdata('logged_in')) {
            $this->portofolio_model->delete($id);
            redirect('imadmin/portofolio');
        } else {
            $this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function project_info($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('project_name', 'Project Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[portofolio.permalink]');
	        $this->form_validation->set_rules('client', 'Client', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('works', 'Works', 'trim|required|xss_clean');

	        if ($this->form_validation->run() == FALSE) {
		    	$data = array(
	            	'pagetitle' => 'Portofolio ~ IMADMIN',
	            	'pos_parent' => 'portofolio',
	            	'pos_child' => 'new_portofolio',
	            	'title' => 'Portofolio',
	            	'subtitle' => 'Create New Portofolio',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/portofolio/cancel/".$id."'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/portofolio">Portofolio</a>', 'New Portofolio'),
	            	'content' => 'imadmin/content/portofolio_new_step1',
	            	'data_portofolio' => $this->portofolio_model->get_portofolio_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
            } else {
	        	if (!$this->upload->do_upload("featured_image")) {
                    $data = array('error' => $this->upload->display_errors());
                    $featured_image = 'teret';
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $this->create_thumb($this->upload->upload_path.$this->upload->file_name);
                    $featured_image = $data['upload_data']['file_name'];
                    $featured_image_thumb = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];
                }
                $data = array(
                	'project_name' => $this->input->post('project_name'),
                    'permalink' => $this->input->post('permalink'),
                	'client' => $this->input->post('client'),
                	'works' => $this->input->post('works'),
                	'featured_image' => $featured_image,
                	'featured_image_thumb' => $featured_image_thumb
                );
	        	$this->portofolio_model->update_portofolio($data, $id);
	        	redirect('imadmin/new-portofolio/'.$id.'/step2', 'refresh');
	        }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function company_info($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->load->library('form_validation');
	        $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
	        $this->form_validation->set_rules('company_profile', 'Company Profile', 'trim|required');

	        if ($this->form_validation->run() == FALSE) {
		    	$data = array(
	            	'pagetitle' => 'Portofolio ~ IMADMIN',
	            	'pos_parent' => 'portofolio',
	            	'pos_child' => 'new_portofolio',
	            	'title' => 'Portofolio',
	            	'subtitle' => 'Create New Portofolio',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/portofolio/cancel/".$id."'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/portofolio">Portofolio</a>', 'New Portofolio'),
	            	'content' => 'imadmin/content/portofolio_new_step2',
	            	'data_portofolio' => $this->portofolio_model->get_portofolio_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
			} else {
	        	if (!$this->upload->do_upload("company_logo")) {
                    $data = array('error' => $this->upload->display_errors());
                    $featured_image = 'teret';
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $this->create_thumb($this->upload->upload_path.$this->upload->file_name);
                    $featured_image = $data['upload_data']['file_name'];
                    $company_logo = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];
                }
                $data = array(
                	'company_profile' => $this->input->post('company_profile'),
                	'company_logo' => $company_logo
                );
	        	$this->portofolio_model->update_portofolio($data, $id);
	        	redirect('imadmin/new-portofolio/'.$id.'/step3', 'refresh');
	        }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function gallery($id = '') {
        if ($this->session->userdata('logged_in')) {
        	$this->load->library('upload');
        	if(isset($_FILES['gallery']['name'])){
		        $files = $_FILES;
		        $cpt = count($_FILES['gallery']['name']);
		        for($i=0; $i<$cpt; $i++) {

		            $_FILES['gallery']['name']= $files['gallery']['name'][$i];
		            $_FILES['gallery']['type']= $files['gallery']['type'][$i];
		            $_FILES['gallery']['tmp_name']= $files['gallery']['tmp_name'][$i];
		            $_FILES['gallery']['error']= $files['gallery']['error'][$i];
		            $_FILES['gallery']['size']= $files['gallery']['size'][$i];    
			        if (!$this->upload->do_upload('gallery')) {
			            $data = array('error' => $this->upload->display_errors());
			        } else {
			            $data = array('upload_data' => $this->upload->data());
			            $this->create_thumb($this->upload->upload_path.$this->upload->file_name);
			            
			            $foto['image'] = $data['upload_data']['file_name'];
			            $foto['thumb'] = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];
			            $foto['portofolio_id'] = $id;
			            $data_gallery[] = $foto;
			        }
		        }
	        	$this->portofolio_model->upload_gallery($data_gallery);
	            redirect('imadmin/new-portofolio/'.$id.'/step4');
	        } else {
		    	$data = array(
	            	'pagetitle' => 'Portofolio ~ IMADMIN',
	            	'pos_parent' => 'portofolio',
	            	'pos_child' => 'new_portofolio',
	            	'title' => 'Portofolio',
	            	'subtitle' => 'Create New Portofolio',
	            	'action' => "<a class='button button-red' href='".base_url()."imadmin/portofolio/cancel/".$id."'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
	            	'breadcrumb' => array('<a href="'.base_url().'imadmin/portofolio">Portofolio</a>', 'New Portofolio'),
	            	'content' => 'imadmin/content/portofolio_new_step3',
	            	'data_portofolio' => $this->portofolio_model->get_portofolio_by_id($id)
	            );
	            $this->load->view('imadmin/template/page', $data);
	        }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    public function project_detail($id = '') {
        if ($this->session->userdata('logged_in')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('project_desc', 'Project Description', 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
    	    	$data = array(
                	'pagetitle' => 'Portofolio ~ IMADMIN',
                	'pos_parent' => 'portofolio',
                	'pos_child' => 'new_portofolio',
                	'title' => 'Portofolio',
                	'subtitle' => 'Create New Portofolio',
                	'action' => "<a class='button button-red' href='".base_url()."imadmin/portofolio/cancel/".$id."'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                	'breadcrumb' => array('<a href="'.base_url().'imadmin/portofolio">Portofolio</a>', 'New Portofolio'),
                	'content' => 'imadmin/content/portofolio_new_step4',
                	'data_portofolio' => $this->portofolio_model->get_portofolio_by_id($id)
                );
                $this->load->view('imadmin/template/page', $data);
            } else {
                $data = array(
                    'project_desc' => $this->input->post('project_desc'),
                    'youtube_video' => $this->input->post('youtube_video'),
                    'status' => '1'
                );
                $this->portofolio_model->update_portofolio($data, $id);
                redirect('imadmin/portofolio', 'refresh');
            }
	    } else {
        	$this->session->set_flashdata("pesan_logout", "<p class='error'>You must login to enter</p>");
            redirect('imadmin/login', 'refresh');
        }
    }

    private function create_thumb($file) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['new_image'] = './assets/uploads';
        $config['maintain_ratio'] = TRUE;
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '_thumb';
        $config['width'] = 450;
        $config['height'] = 300;
        $config['master_dim'] = 'auto';
        $config['maintain_ratio'] = TRUE;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

}

?>
