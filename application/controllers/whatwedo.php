<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Whatwedo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('blog_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'whatwedo',
            'title' => 'What We Do | Inncomedia',
            'content' => 'content/whatwedo',
            'plugins_css' => array('assets/plugins/cd-tabs/cd-tabs.css'),
            'plugins_js' => array('assets/plugins/cd-tabs/cd-tabs.js')
        );
        $this->load->view('template/page', $data);
    }

}

?>
