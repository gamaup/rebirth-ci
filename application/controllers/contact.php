<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mailbox_model', '', TRUE);
    }

    public function index() {
        $this->load->library('form_validation');
        $data = array(
            'pos' => 'contact',
            'title' => 'Contact Us | Inncomedia',
            'content' => 'content/contact',
            'plugins_css' => array('assets/plugins/mapbox/mapbox.css'),
            'plugins_js' => array('assets/plugins/mapbox/mapbox.js')
        );
        $this->load->view('template/page', $data);
    }

    public function send() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters("", "");
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|numeric');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('status_form', "<p class='status-error'>".validation_errors()."</p>");
            redirect('contact', 'refresh');
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('message'),
                'date' => date("Y-m-d H:i:s")
            );
            $this->mailbox_model->send($data);
            $this->session->set_flashdata('status_form', "<p class='status-success'>Your message was sent. We'll reply as soon as possible.</p>");
            redirect('contact', 'refresh');
        }
    }
}

?>
