<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->model('portofolio_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'about',
            'title' => 'About Us | Inncomedia',
            'content' => 'content/about'
        );
        $this->load->view('template/page', $data);
    }

}

?>
