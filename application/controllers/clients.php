<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('blog_model', '', TRUE);
        $this->load->model('testimoni_model', '', TRUE);
        $this->load->model('clients_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'clients',
            'title' => 'Our Clients | Inncomedia',
            'content' => 'content/clients',
            'plugins_css' => array('assets/plugins/owl-carousel/owl.carousel.css', 'assets/plugins/owl-carousel/owl.theme.css'),
            'plugins_js' => array('assets/plugins/owl-carousel/owl.carousel.min.js'),
            'data_testimoni' => $this->testimoni_model->get_all_testimoni(),
            	'data_clients' => $this->clients_model->get_all_clients()
        );
        $this->load->view('template/page', $data);
    }

}

?>
