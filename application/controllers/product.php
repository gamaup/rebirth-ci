<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->model('portofolio_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'product',
            'title' => 'Product | Inncomedia',
            // 'content' => 'content/product',
            'content' => 'content/product-comingsoon',
            'plugins_css' => array('assets/plugins/owl-carousel/owl.carousel.css', 'assets/plugins/owl-carousel/owl.theme.css'),
            'plugins_js' => array('assets/plugins/owl-carousel/owl.carousel.min.js')
        );
        $this->load->view('template/page', $data);
    }

}

?>
