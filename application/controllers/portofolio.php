<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Portofolio extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('portofolio_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'portofolio',
            'title' => 'Portofolio | Inncomedia',
            'content' => 'content/portofolio',
            'data_portofolio' => $this->portofolio_model->get_all_portofolio()
        );
        $this->load->view('template/page', $data);
    }

    public function project($permalink = '') {
        if ($this->portofolio_model->check_portofolio_by_permalink($permalink) == 0) {show_404();}
        $portofolio_id = $this->portofolio_model->get_id_by_permalink($permalink);
        $data = array(
            'pos' => 'portofolio',
            'title' => 'Portofolio | Inncomedia',
            'content' => 'content/portofolio-view',
            'plugins_css' => array('assets/plugins/owl-carousel/owl.carousel.css', 'assets/plugins/owl-carousel/owl.theme.css', 'assets/plugins/swipebox/css/swipebox.css'),
            'plugins_js' => array('assets/plugins/owl-carousel/owl.carousel.min.js','assets/plugins/swipebox/js/jquery.swipebox.js'),
            'data_portofolio' => $this->portofolio_model->get_portofolio_by_permalink($permalink),
            'data_gallery' => $this->portofolio_model->get_portofolio_gallery_by_id($portofolio_id)
        );
        $this->load->view('template/page', $data);
    }

}

?>
