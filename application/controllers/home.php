<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('blog_model', '', TRUE);
        $this->load->model('portofolio_model', '', TRUE);
        $this->load->model('testimoni_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'home',
            'title' => 'Inncomedia',
            'content' => 'content/home',
            'plugins_css' => array('assets/plugins/owl-carousel/owl.carousel.css', 'assets/plugins/owl-carousel/owl.theme.css'),
            'plugins_js' => array('assets/plugins/owl-carousel/owl.carousel.min.js'),
            'data_portofolio' => $this->portofolio_model->get_recent_portofolio(),
            'data_testimoni' => $this->testimoni_model->get_all_testimoni()
        );
        $this->load->view('template/page', $data);
    }

}

?>
