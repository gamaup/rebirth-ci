<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('blog_model', '', TRUE);
    }

    public function index() {
        $data = array(
            'pos' => 'blog',
            'title' => 'Blog | Inncomedia',
            'content' => 'content/blog',
            'data_blog' => $this->blog_model->get_all_blog()
        );
        $this->load->view('template/page', $data);
    }

    public function post($permalink = '') {
        if ($this->blog_model->check_blog_by_permalink($permalink) == 0) {show_404();}
        $post = $this->blog_model->get_blog_by_permalink($permalink);
        $meta = array(
            'og:image' => base_url().'assets/uploads/'.$post[0]->featured_image,
            'og:title' => $post[0]->title,
            'og:url' => base_url().'blog/'.$post[0]->permalink,
            'og:site_name' => 'Inncomedia Blog',
            'og:type' => 'blog',
        );
        // print_r($ini);
        $data = array(
            'pos' => 'blog',
            'title' => $post[0]->title.' ~ Inncomedia Blog',
            'content' => 'content/blog_view',
            'meta' => $meta,
            'data_blog' => $this->blog_model->get_blog_by_permalink($permalink)
        );
        $this->load->view('template/page', $data);
    }

}

?>
