<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'im404';
$route['imadmin'] = "imadmin/dashboard";
$route['imadmin/new-portofolio/(:num)/step1'] = 'imadmin/portofolio/project_info/$1';
$route['imadmin/new-portofolio/(:num)/step2'] = 'imadmin/portofolio/company_info/$1';
$route['imadmin/new-portofolio/(:num)/step3'] = 'imadmin/portofolio/gallery/$1';
$route['imadmin/new-portofolio/(:num)/step4'] = 'imadmin/portofolio/project_detail/$1';

$route['blog/(:any)'] = 'blog/post/$1';
$route['project/(:any)'] = 'portofolio/project/$1';
// $route['article'] = "front/article";
// $route['article/(:num)'] = "front/article/$1";
// $route['promo'] = "front/promo";
// $route['promo/(:any)'] = "front/promo/$1";
// $route['package'] = "front/package";
// $route['package/(:num)'] = "front/package/$1";
// $route['gallery'] = "front/gallery";


/* End of file routes.php */
/* Location: ./application/config/routes.php */