<section class='slider'>
	<img src='<?= base_url() ?>assets/images/banner-1.jpg'/>
</section>
<section class='whatwedo-desc'>
	<p class='container'>Kami menawarkan solusi digital tanpa batas, seperti: website, software, aplikasi mobile, desain grafis dan multimedia, IT consultant, layanan jaringan, pengadaan barang hingga layanan hosting yang lengkap untuk bisnis dan gaya hidup Anda. Klik pada icon-icon berikut untuk detail jasa yang kami tawarkan:</p>
</section>
<section class='whatwedo-wrapper'>
    <div class="cd-tabs">
		<nav>
			<div class='container'>
				<ul class="cd-tabs-navigation">
					<li><a data-content="web-development" class="selected" href="#0"><img src='<?= base_url() ?>assets/images/svg/website.svg'/></a></li>
					<li><a data-content="software-development" href="#0"><img src='<?= base_url() ?>assets/images/svg/software.svg'/></a></a></li>
					<li><a data-content="mobile-app-development" href="#0"><img src='<?= base_url() ?>assets/images/svg/mobile.svg'/></a></a></li>
					<li><a data-content="multimedia" href="#0"><img src='<?= base_url() ?>assets/images/svg/multimedia.svg'/></a></a></li>
					<li><a data-content="socialmedia" href="#0"><img src='<?= base_url() ?>assets/images/svg/marketing.svg'/></a></a></li>
					<li><a data-content="network-infrastructure" href="#0"><img src='<?= base_url() ?>assets/images/svg/network.svg'/></a></a></li>
					<li><a data-content="hardware-procurement" href="#0"><img src='<?= base_url() ?>assets/images/svg/hardware.svg'/></a></a></li>
					<li><a data-content="hosting" href="#0"><img src='<?= base_url() ?>assets/images/svg/hosting.svg'/></a></a></li>
				</ul>
			</div>
		</nav>

		<ul class="cd-tabs-content">
			<li data-content="web-development" class="selected">
				<h1 class='whatwedo-title'>WebsiteDevelopment</h1>
				<div class='whatwedo-service ws-web'>
					<div class='row container'>
						<div class='left'>
							<h3>Web Design</h3>
							<p>Mendesain dan mengembangkan sebuah website membutuhkan imajinasi dan kreativitas yang tinggi, itulah sebabnya mengapa Inncomedia memiliki tim webdesigner yang kompeten dan mempertimbangkan strategi bisnis, branding, teknologi dan hasil yang Anda inginkan pada setiap langkah dari proses desain. Kami memastikan bahwa desain website yang kami buat tidak hanya ditujukan kepada pengunjung setia website Anda, namun juga dibuat khusus untuk mendapatkan pengunjung baru yang potensial untuk menjadi partner bisnis Anda, sekarang dan di masa yang akan datang.</p>
						</div>
						<div class='right'>
							<img class='illustration' src='assets/images/webdesign-illustration.png'/>
						</div>
					</div>
					<div class='row container'>
						<div class='right'>
							<h3>Web Development</h3>
							<p>Inncomedia memahami bahwa setiap bisnis memiliki tujuan yang berbeda, sehingga setiap pendekatan yang kami lakukan kami sesuaikan dengan kebutuhan bisnis Anda. Sebagai partner Anda yang professional, kami berusaha mengembangkan online-platform yang menyesuaikan dengan setiap kebutuhan bisnis Anda untuk terus dapat berkembang.</p>
							<p>Inncomedia menawarkan berbagai macam pengembangan website. Dari company profile, website personal, website instansi, e-commerce, blog, dan berbagai macam web kustom lainnya.</p>
						</div>
						<div class='left'>
							<img class='illustration' src='assets/images/webdev-illustration.png'/>
						</div>
					</div>
					<div class='why-us container'>
						<h2><span>Kenapa Harus Kami?</span></h2>
						<div class='row'>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> Responsive</h3>
								<p>Setiap website yang kami buat, akan kami bangun seindah mungkin. Dengan pemikiran ini, kami menerapkan teknik responsif dan teknologi web terbaru ke dalam setiap website yang kami bangun. Sehingga terlepas dari berapapun resolusi atau ukuran perangkat yang digunakan, hasil akhirnya akan selalu sempurna.</p>
							</div>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> Mobile Enhanced</h3>
								<p>Bukan hanya responsif, kami membangun website yang memiliki navigasi yang sepenuhnya unik, baik menu maupun konten untuk konversi ke versi mobile yang lebih optimal.</p>
								<p>Dengan perangkat mobile yang telah tumbuh dengan pesat sebagai platform pilihan untuk mengakses internet, kami tidak akan membiarkan Anda hanya puas dengan pengalaman mobile yang biasa.</p>
							</div>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> Cross Browser &amp; Platform</h3>
								<p>Kami memastikan bahwa setiap website yang kami bangun akan optimal di setiap browser dan platform yang berbeda.</p>
							</div>
						</div>
						<div class='row'>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> User Interface</h3>
								<p>Dalam setiap user-interface yang kami rancang, kami mengeksplorasi teknologi web terbaru dan metode penyampaian pada setiap komponen user-interface. Hal ini membuat nuansa keseluruhan interface yang dibuat memiliki estetika yang efektif dan modern.</p>
							</div>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> User Experience</h3>
								<p>Kami menaruh perhatian besar untuk menyediakan antarmuka yang paling intuitif. Dan dengan mempertimbangkan konteks dan target pengunjung, kami menciptakan user-experience yang tidak hanya menyenangkan namun juga mengesankan.</p>
							</div>
							<div class='why-us-item'>
								<h3><i class="fa fa-check"></i> Social Media Integrations</h3>
								<p>Kami dapat mengintegrasikan jaringan media sosial ke dalam website Anda seperti: Facebook, Twitter, Instagram, dan lain-lain.</p>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li data-content="software-development">
				<h1 class='whatwedo-title'>SoftwareDevelopment</h1>
				<div class='whatwedo-service ws-software'>
					<div class='row container'>
						<div class='left'>
							<h3>Web Application</h3>
							<p>Inncomedia memiliki reputasi untuk membuat software berbasis web-application inovatif yang memenuhi bahkan melampaui harapan. Kami menawarkan kepada Anda web-application dengan tingkat kegunaan yang tinggi, skalabilitas dan kompatibilitas lengkap baik browser maupun platform apapun.</p>
						</div>
						<div class='right'>
							<img class='illustration' src='assets/images/illustration-software.png'/>
						</div>
					</div>
					<div class='row container'>
						<div class='right'>
							<p>Masing-masing modul yang kami rancang dibuat khusus untuk dapat masuk ke dalam kerangka kerja yang memenuhi aspek kegunaan, kinerja, keamanan dan audit. Karena setiap bentuk bisnis adalah unik dengan kebutuhannya sendiri, kami menginvestasikan banyak waktu mencoba untuk belajar bagaimana bisnis Anda bekerja sehingga kami dapat mengusulkan opsi yang unik dan inovatif untuk bisnis Anda.</p>
						</div>
						<div class='left'>
							<img class='illustration' src='assets/images/illustration-software-2.png'/>
						</div>
					</div>
				</div>
			</li>
			<li data-content="mobile-app-development">
				<h1 class='whatwedo-title'>MobileApps</h1>
				<div class='whatwedo-service ws-software'>
					<div class='row container'>
						<div class='right'>
							<p>Perangkat mobile telah menjadi sarana komunikasi universal. Dengan kebanyakan perangkat mobile yang tersedia di pasar, dengan sistem operasi yang berbeda seperti Android, iOS, Windows atau Blackberry OS, maka kebutuhan untuk mengembangkan aplikasi mobile yang menarik dan user-friendly telah sangat meningkat. Kami memahami hal ini dan karenanya, kami berjanji untuk memberikan aplikasi mobile dengan kinerja yang tinggi namun praktis serta menarik bagi pengguna.</p>
							<p>Inncomedia menawarkan solusi tepat untuk bisnis Anda - apakah itu merancang versi mobile dari situs Anda, membuat website Anda responsif terhadap semua perangkat yang berbeda atau mengembangkan aplikasi kustom mobile untuk Google Android, kita dapat mengelola seluruh proses untuk Anda dan memastikan bisnis Anda siap untuk masa depan.</p>
						</div>
						<div class='left'>
							<img class='illustration' src='assets/images/illustration-mobile.png'/>
						</div>
					</div>
				</div>
			</li>
			<li data-content="multimedia">
				<h1 class='whatwedo-title'>Multimedia</h1>
				<p class='container'>Kami siap membantu usaha Anda dalam hal branding &amp; identity dengan membuat berbagai macam kebutuhan multimedia seperti Logo, Video Profile, Desain Banner, dan berbagai macam desain grafis lainnya.</p>
			</li>
			<li data-content="socialmedia">
				<h1 class='whatwedo-title'>SocialMediaMarketing</h1>
				<div class='whatwedo-service ws-software'>
					<div class='row container'>
						<div class='right'>
							<p>Inncomedia IT Consultant membantu perusahaan berpikir ke depan. Bisnis hari ini berkembang lebih cepat , dan besok ada akan menjadi tantangan yang kita bahkan tidak bisa membayangkan. Tetapi untuk bertahan dan berkembang, kita harus membayangkan masa depan. Kita harus mengantisipasinya. Bahkan saat kita membantu Anda memecahkan tantangan bisnis Anda hari ini, kita berpikir tentang masa depan - dan bagaimana kita dapat membawa Anda ke sana. Yang terpenting adalah apa yang memberi Anda dapat bersaing dimasa yang akan datang.</p>
						</div>
						<div class='left'>
							<img class='illustration' src='assets/images/illustration-itconsultant.png'/>
						</div>
					</div>
				</div>
			</li>
			<li data-content="network-infrastructure">
				<h1 class='whatwedo-title'>NetworkInfrastructure</h1>
				<div class='whatwedo-service ws-software'>
					<div class='row container'>
						<div class='right'>
							<p>Bayangkan masa depan di mana jaringan Anda merespon kebutuhan bisnis Anda secara real time. Inncomedia akan mengubah cara Anda penyediaan dan mengelola layanan jaringan Anda.</p>
							<p>Layanan yang kami sediakan :</p>
							<h3>VPN Service</h3>
							<p>Dukungan suara, video dan aplikasi bisnis pada satu jaringan VPN. Membantu mengamankan komunikasi Anda dengan virtual private network (VPN).</p>
							<h3>Wi-Fi Solutions</h3>
							<p>Kami dapat membantu menciptakan Wi-Fi hotspot solution yang sesuai dengan kebutuhan dan anggaran Anda.</p>
						</div>
						<div class='left'>
							<img class='illustration' src='assets/images/illustration-network.png'/>
						</div>
					</div>
				</div>
			</li>
			<li data-content="hardware-procurement">
				<h1 class='whatwedo-title'>HardwareProcurement</h1>
				<p class='container'>Kami dapat membantu Anda dalam hal pengadaan alat yang dibutuhkan pada perusahaan Anda, seperti pengadaan komputer dll.</p>
			</li>
			<li data-content="hosting">
				<h1 class='whatwedo-title'>Hosting</h1>
				<div class='whatwedo-service ws-hosting'>
					<div class='row container'>
						<div class='left'>
							<p>Inncomedia menyediakan layanan web hosting yang sangat terjangkau dan mendukung php, mysql, terintegrasi dengan cPanel yang umum digunakan oleh webhosting provider di seluruh dunia. Dengan uptime paling tinggi, kami memberikan garansi uptime 99 persen.</p>
							<p>Menghadirkan layanan hosting murah tidak berarti kami mempertaruhkan kualitas. Kami menggunakan teknologi dan hardware terbaik. Memberikan jaminan kepuasan pelanggan yang maksimal dalam menggunakan layanan hosting kami yang murah dan terjangkau.</p>
						</div>
						<div class='right'>
							<img class='illustration' src='assets/images/illustration-hosting.png'/>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</section>
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Your Project Now!</span></h1>
    </a>
</section>
<?php $this->load->view('content/blog_recent') ?>