<section class='latest-work'>
    <div class='latest-work-title'>
        <h1>Our Latest Work</h1>
        <a href='#'>Page 1 of 1</a>
    </div>
    <div class='latest-work-wrapper'>
        <?php foreach ($data_portofolio as $p) { ?>
        <div class='latest-work-item' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $p->featured_image_thumb ?>)'>
            <div class='overlay'>
                <h4><a href='<?= base_url('project').'/'.$p->permalink ?>'>View Details</a></h4>
            </div>
        </div>
        <?php } ?>
    </div>
</section>