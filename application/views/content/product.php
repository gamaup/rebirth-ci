<section class='product-nav-wrapper'>
    <div id='product-nav-konten'>
        <div id='product-nav-body' class='owl-carousel'>
            <div class='product-nav-item'>
                <img src="assets/images/study.png">
                <h4>SHERMAN</h4>
            </div>
            <div class='product-nav-item'>
                <img src="assets/images/news.png">
                <h4>ADENS</h4>
            </div>
            <div class='product-nav-item'>
                <img src="assets/images/screen.png">
                <h4>BONITA</h4>
            </div>
            <div class='product-nav-item'>
                <img src="assets/images/keyman.png">
                <h4>KEYMAN</h4>
            </div>
            <div class='product-nav-item'>
                <img src="assets/images/connections.png">
                <h4>KONGURITY</h4>
            </div>
        </div>
    </div>
</section>
<div id='product-nav-button-wrapper'>
        <div class='product-nav-button'>
            <!-- <img src="assets/images/up.png"> -->
        </div>
</div>
<section class='product-outer-wrapper product-1'>
    <div class='container'>
        <div class='product-konten'>
            <h2>Lorem Ipsum</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>
        <div class='product-image'>
            <img src="assets/images/produk.png">
        </div>
    </div>
</section>
<section class='product-outer-wrapper'>
    <div class='container'>
        <div class='product-konten'>
            <h2>Lorem Ipsum</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>
        <div class='product-image'>
            <img src="assets/images/produk.png">
        </div>
    </div>
</section>
<section class='product-outer-wrapper'>
    <div class='container'>
        <div class='product-konten'>
            <h2>Lorem Ipsum</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>
        <div class='product-image'>
            <img src="assets/images/produk.png">
        </div>
    </div>
</section>
<section class='product-outer-wrapper'>
    <div class='container'>
        <div class='product-konten'>
            <h2>Lorem Ipsum</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>
        <div class='product-image'>
            <img src="assets/images/produk.png">
        </div>
    </div>
</section>
<section class='product-outer-wrapper'>
    <div class='container'>
        <div class='product-konten'>
            <h2>Lorem Ipsum</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>
        <div class='product-image'>
            <img src="assets/images/produk.png">
        </div>
    </div>
</section>