<section class='slider'>
    <img src='<?= base_url() ?>assets/images/banner-1.jpg'/>
</section>
<section class='whatwedo'>
    <p class="container">Inncomedia adalah creative digital agency &amp; IT solutions yang berbasis di Yogyakarta. Memiliki fokus pada penyediaan solusi digital yang kreatif, inovatif, dan berkesinambungan untuk bertujuan sebagai suplemen dalam menghadapi persaingan bisnis di era digital serta tuntutan dunia gaya hidup yang terus berkembang. Kami menawarkan solusi digital tanpa batas, seperti: website, software, aplikasi mobile, desain grafis dan multimedia, IT consultant, layanan jaringan, pengadaan barang hingga layanan hosting yang lengkap untuk bisnis dan gaya hidup Anda.</p>
    <div class='container'>
        <div id="whatwedo-slide" class="owl-carousel">
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/website.svg' alt='Website Development'/>
                <h3>Website Development</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/software.svg' alt='Software Development'/>
                <h3>Software Development</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/mobile.svg' alt='Mobile App Development'/>
                <h3>Mobile App Development</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/multimedia.svg' alt='Multimedia'/>
                <h3>Multimedia</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/consultant.svg' alt='IT Support &amp; Consultant'/>
                <h3>IT Support &amp; Consultant</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/network.svg' alt='Network Infrastruture'/>
                <h3>Network Infrastructure</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/hardware.svg' alt='Hardware Procurement'/>
                <h3>Hardware Procurement</h3>
            </div>
            <div class='whatwedo-item'>
                <img src='<?= base_url() ?>assets/images/svg/hosting.svg' alt='Hosting'/>
                <h3>Hosting</h3>
            </div>
        </div>
    </div>
    <div class='whatwedo-prev'><i class="fa fa-angle-left"></i>
    </div>
    <div class='whatwedo-next'><i class="fa fa-angle-right"></i>
    </div>
    
    <h2><a href='<?= base_url() ?>whatwedo'>View All Our Service Details</a></h2>
</section>
<section class='latest-work'>
    <div class='latest-work-title'>
        <h1>Our Latest Work</h1>
        <a href='<?= base_url('portofolio') ?>'>View All</a>
    </div>
    <div class='latest-work-wrapper' style='min-height:200px;'>
        <?php foreach ($data_portofolio as $p) { ?>
        <div class='latest-work-item' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $p->featured_image_thumb ?>)'>
            <div class='overlay'>
                <h4><a href='<?= base_url() ?>project/<?= $p->permalink ?>'>View Details</a></h4>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<section class='testimoni container'>
    <div id="testimoni-slide" class="owl-carousel">
        <?php foreach($data_testimoni as $t) { ?>
        <div class='testimoni-item'>
            <p>"<?= $t->testimoni ?>"</p>
            <h3><?= $t->name ?></h3>
            <h5><?= $t->position ?></h5>
        </div>
        <?php } ?>
    </div>
</section>
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Your Project Now!</span></h1>
    </a>
</section>
<?php $this->load->view('content/blog_recent') ?>