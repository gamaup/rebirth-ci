<section class='blog-slider'></section>
<section class='blog-wrap container'>
    <?php foreach ($data_blog as $b) { ?>
    <div class='blog-container'>
        <div class='blog-image' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $b->featured_image_thumb ?>)'>
        </div>
        <div class='blog-detail'>
            <a href='<?= base_url() ?>blog/<?= $b->permalink ?>'><h3 class='blog-title'><?= $b->title ?></h3></a>
            <h5 class='post-date'><?= date("D, d M Y", strtotime($b->date)); ?></h5>
            <p class='blog-summary'><?= $b->summary ?></p>
            <a class='readmore' href='<?= base_url() ?>blog/<?= $b->permalink ?>'>Read More <i class="fa fa-chevron-circle-right"></i></a>
        </div>
    </div>
    <?php } ?>
</section>
<!-- <section class='paging small-container'>
    Page: 
    <a href='#' class='active'>1</a>
    <a href='#'>2</a>
    <a href='#'>3</a>
    <a href='#'>4</a>                
</section> -->
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Your Project Now!</span></h1>
    </a>
</section>