<?= $this->load->view('content/map') ?>
<section class="contact container">
    <div class="container">
        <div id="contact-kiri">
            <?= $this->session->flashdata('status_form') ?>
            <?= form_open(base_url()."contact/send") ?>
                <label>Your Name</label>
                <input type="text" name="name">
                <div class='left'>
                    <label>Your Email</label>
                    <input type="text" name="email">
                </div>
                <div class='right'>
                    <label>Your Phone</label>
                    <input type="text" name="phone">
                </div>
                <label>Subject</label>
                <input type="text" name="subject">
                <label>Message</label>
                <textarea spellcheck="false" name="message"></textarea>
                <?= form_error() ?>
                <input type="submit" value="Send"> <span style='display:inline-block; margin-left:20px; font-size:10pt; line-height:20px; color: #9a1e2b;'>All fields are required</span>
            <?= form_close() ?>
        </div>
        <div id="contact-kanan">
            <h3>Address</h3>
            <p>Perum Griya Perwita Wisata (GPW) CU4 Utara. Jalan Kaliurang KM13, Sleman - DIY</p>
            <h3>Contact Details</h3>
            <p>+62-857-6145-6815 <br>
            cs@inncomedia.com</p>
            <h3>Keep in Touch</h3>
            <div class="social">
                <a href='https://www.facebook.com/inncomedia' target='_blank'>
                    <img src="<?= base_url() ?>assets/images/facebook.png" alt="">
                    <div class="socialbox">
                        <p class="text"><img src="<?= base_url() ?>assets/images/facebook.png"></p>
                    </div>
                </a>
            </div>
            <div class="social">
                <a href='https://twitter.com/inncomedia' target='_blank'>
                    <img src="<?= base_url() ?>assets/images/twitter.png" alt="">
                    <div class="socialbox">
                        <p class="text"><img src="<?= base_url() ?>assets/images/twitter.png"></p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>