<section class='slider'>
	<img src='<?= base_url() ?>assets/images/banner-1.jpg'/>
</section>
<section class='about-us small-container'>
	<div class='row'>
		<div class='left'>
			<h3>Innovative, Creative, and Outstanding</h3>
		    <p>Inncomedia adalah creative digital agency & IT solutions yang berbasis di Yogyakarta. Memiliki fokus pada penyediaan solusi digital yang kreatif, inovatif, dan berkesinambungan untuk bertujuan sebagai suplemen dalam menghadapi persaingan bisnis di era digital serta tuntutan dunia gaya hidup yang terus berkembang.</p>
		</div>
	</div>
	<div class='row'>
		<div class='right'>
			<h3>Offers Limitless Digital Solutions</h3>
		    <p>Kami menawarkan solusi digital tanpa batas, seperti: website, software, aplikasi mobile, desain grafis dan multimedia, IT consultant, layanan jaringan, pengadaan barang hingga layanan hosting yang lengkap untuk bisnis dan gaya hidup Anda. Kami memahami bagaimana membangun solusi digital yang besar, dimana bukan hal yang mudah, tidak hanya memerlukan keahlian teknis, tapi juga pandangan yang tajam dalam hal estetika &amp; kegunaan, serta eksekusi tanpa cela.</p>
		</div>
	</div>
	<div class='row'>
		<div class='left'>
			<h3>A Team of Young, Passionate and Dedicated Individuals</h3>
		    <p>Kami merupakan tim developer, desainer dan teknisi yang muda dan terus berkembang serta tanpa henti berinovasi dalam pesatnya perkembangan dunia teknologi. Selalu memiliki semangat untuk bekerja dengan klien untuk mengubah ide mereka menjadi kenyataan.</p>
    		<p>Sejak 2013 kami telah menggabungkan tujuan bisnis klien kami dengan harapan pelanggan mereka serta dipadukan dengan kreativitas kami menjadi sebuah produk digital yang berkesinambungan.</p>
		</div>
	</div>
</section>