<section class='recent-blog'>
    <div class='container'>
        <h1>Recent Blog Post</h1>
        <?php $data_recent_blog = $this->blog_model->get_recent_blog(); ?>
        <?php foreach ($data_recent_blog as $b) { ?>
        <div class='col-4'>
            <div class='recent-blog-item'>
                <div class='blog-image' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $b->featured_image_thumb ?>)'>
                    <a href='<?= base_url() ?>blog/<?= $b->permalink ?>'></a>
                </div>
                <h4><a href='<?= base_url() ?>blog/<?= $b->permalink ?>'><?= $b->title ?></a></h4>
                <p><?= $b->summary ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
</section>