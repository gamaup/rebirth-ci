<?php foreach ($data_blog as $b) { ?>
<section class='featured-image' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $b->featured_image ?>)'>
    <div class='overlay'>
        <div class='post-info'>
            <h2 class='title small-container'><?= $b->title ?></h2>
            <div class='post-detail small-container'>
                <span class='post-date'><?= date("D, d F Y", strtotime($b->date)); ?></span><span class='post-author'><?= $b->author ?></span>
            </div>
        </div>
    </div>
</section>
<section class='blog-content small-container'>
    <?= $b->isi ?>
</section>
<div id="disqus_thread" class='small-container' style='margin-bottom: 40px;'></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'inncomediablog';
    var disqus_title = '<?= $b->title ?>';
    var disqus_identifier = '<?= $b->id; ?>';
    var disqus_url = '<?= base_url()."blog/".$b->permalink ?>';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
<?php } ?>
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Your Project Now!</span></h1>
    </a>
</section>
<?php $this->load->view('content/blog_recent') ?>