<?php foreach ($data_portofolio as $p) { ?>
<section class='portofolio-landing' style='background-image:url(<?= base_url().'assets/uploads/'.$p->featured_image ?>)'>
    <div>
        <h1 class='project-title container'>
            <?= $p->project_name ?>
        </h1>
        <div class='project-client container'>
            <h5>&#8212; client &#8212;</h5>
            <h4><?= $p->client ?></h4>
        </div>
        <div class='project-work container'>
            <h5>&#8212; works &#8212;</h5>
            <h4><?= $p->works ?></h4>
        </div>
    </div>
</section>
<section class='client-profile small-container'>
    <img class='client-logo' src='<?= base_url() ?>assets/uploads/<?= $p->company_logo ?>'/>
    <?= $p->company_profile ?>
</section>
<section class='project-snapshot'>
    <div id="project-snapshot" class="owl-carousel">
        <?php foreach ($data_gallery as $g) { ?>
            <a href="<?= base_url() ?>assets/uploads/<?= $g->image ?>" class="swipebox" title="Project Snapshot">
                <div class='project-snapshot-item' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $g->thumb ?>)'>
                </div>
            </a>
        <?php } ?>
    </div>
</section>
<section class='project-story small-container'>
   <?= $p->project_desc ?>
</section>
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Making Your Own Project</span></h1>
    </a>
</section>
<?php } ?>