<div class='tag-clients'>
    <h1>
        Clients We Have Worked With
    </h1>
</div>
<section class='container'>
    <ul class='container-view-clients'>
    	<?php foreach ($data_clients as $c) { ?>
        <li>
            <img src="<?= base_url() ?>assets/uploads/<?= $c->image ?>">
            <label><?= $c->desc ?></label>
        </li>
        <?php } ?>
    </ul>
</section>
<div class='testi'>
    <h1 >
        Testimonial
    </h1>
</div>
<section class='testimoni container'>
    <div id="testimoni-slide" class="owl-carousel">
        <?php foreach($data_testimoni as $t) { ?>
        <div class='testimoni-item'>
            <p>"<?= $t->testimoni ?>"</p>
            <h3><?= $t->name ?></h3>
            <h5><?= $t->position ?></h5>
        </div>
        <?php } ?>
    </div>
</section>
<section class='start-project'>
    <a href='<?= base_url('contact') ?>'>
        <h1><span>Let's Start Your Project Now!</span></h1>
    </a>
</section>
<?php $this->load->view('content/blog_recent') ?>