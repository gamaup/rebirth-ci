<section id='map'>
</section>
		<script>
		var map = L.mapbox.map('map', 'gamaup.khgggh7e', {
		    scrollWheelZoom: false
		}).setView([-7.696, 110.419], 15);

		map.markerLayer.on('click', function(e) {
	        map.panTo(e.layer.getLatLng());
	    });

		var geoJson = [{
		    "type": "Feature",
		    "geometry": {
		        "type": "Point",
		        "coordinates": [110.419, -7.696]
		    },
		    "properties": {
		        "title": "Inncomedia HQ",
		        "icon": {
                            "iconUrl": "assets/images/mapicon.png",
		            "iconSize": [80, 80], // size of the icon
		            "iconAnchor": [40, 70], // point of the icon which will correspond to marker's location
		            "popupAnchor": [0, -70], // point from which the popup should open relative to the iconAnchor
		            "className": "dot"
		        }
		    }
		}];

		// Set a custom icon on each marker based on feature properties
		map.markerLayer.on('layeradd', function(e) {
		    var marker = e.layer,
		        feature = marker.feature;

		    marker.setIcon(L.icon(feature.properties.icon));
		});

		// Add features to the map
		map.markerLayer.setGeoJSON(geoJson);

		</script>