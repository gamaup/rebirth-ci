<div class='row'>
    <div class='col-2'>
        <div class='panel'>
            <div class='steps-head steps-red'>
                <div class='step step-done'>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-current'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <span class='step-dot'></span>
                </div>
            </div>
            <div class='panel-head'>
                <h3>Step 2: Company Info</h3>
            </div>
            <?php foreach ($data_portofolio as $p) { ?>
            <?= form_open_multipart(base_url()."imadmin/portofolio/company_info/".$p->id)?>
            <div class='panel-body'>
                <div class='input-row inline'>
                    <h5>Company Name :</h5>
                    <input type='text' value='<?= $p->client ?>' disabled/>
                </div>
                <div class='input-row inline'>
				    <h5>Company Logo :</h5>
				    <div class='input-file'>
				        <input type='text'/>
				        <h4 class='button button-blue'>Browse</h4>
				        <input type="file" name='company_logo'/>
				    </div>
                    <p class="helper">Format PNG, background transparan. Maksimal ukuran 2MB</p>
				</div>
                <div class='input-row'>
				    <h5>Company Profile :</h5>
				    <textarea id='wysiwyg' name='company_profile'><?= $p->company_profile ?></textarea>
				</div>
                <script>
				CKEDITOR.replace('wysiwyg');
				</script>
                <div class='input-row submit'>
                    <input type='submit' value='Next Step' class='button button-blue'/>
                </div>
                <?= form_close()?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>