<div class='row gallery'>
	<?php foreach ($data_clients as $c) { ?>
    <div class='gallery-item'>
        <h5 class='gallery-text'>
            <?= $c->desc ?>
        </h5>
        <div class='gallery-image' style='background-image: url(<?= base_url() ?>assets/uploads/<?= $c->image ?>)'>
        </div>
        <div class='gallery-tools'>
            <i class='fa fa-eye tipt' title="View"></i>
            <i class='fa fa-pencil tipt' title="Edit"></i>
            <a href='<?= base_url() ?>imadmin/clients/delete/<?= $c->id ?>'><i class='fa fa-trash-o tipt' title="Delete"></i></a>
        </div>
    </div>
    <?php } ?>
</div>