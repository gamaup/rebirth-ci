<div class='row'>
    <div class='col-2'>
        <div class='panel'>
            <div class='steps-head steps-red'>
                <div class='step step-done'>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-done'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-done'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-current'>
                    <div class='back-line-left'></div>
                    <span class='step-dot'></span>
                </div>
            </div>
            <div class='panel-head'>
                <h3>Step 4: Project Details</h3>
            </div>
            <?php foreach ($data_portofolio as $p) { ?>
            <?= form_open(base_url()."imadmin/portofolio/project_detail/".$p->id)?>
            <div class='panel-body'>
                <div class='input-row'>
				    <h5>Project Description :</h5>
				    <textarea id='wysiwyg' name='project_desc'><?= $p->project_desc ?></textarea>
				</div>
                <script>
				CKEDITOR.replace('wysiwyg');
				</script>
                <div class='input-row inline'>
                    <h5>Video (optional) :</h5>
                    <input type='text' value='<?= $p->youtube_video ?>' name='youtube_video'/>
                    <p class='helper'>Link video Youtube</p>
                </div>
                <div class='input-row submit'>
                    <input type='submit' value='Save' class='button button-blue'/>
                </div>
                <?= form_close()?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>