<div class='row'>
    <div class='col-2'>
        <div class='panel'>
            <div class='steps-head steps-red'>
                <div class='step step-done'>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-done'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step step-current'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <span class='step-dot'></span>
                </div>
            </div>
            <div class='panel-head'>
                <h3>Step 3: Screenshot/Photo</h3>
            </div>
            <?php foreach ($data_portofolio as $p) { ?>
            <?= form_open_multipart(base_url()."imadmin/portofolio/gallery/".$p->id)?>
            <div class='panel-body'>
                <div class='input-row inline'>
				    <h5>Upload :</h5>
				    <div class='input-file'>
				        <input type='text'/>
				        <h4 class='button button-blue'>Browse</h4>
				        <input type="file" name='gallery[]' multiple/>
				    </div>
                    <p class="helper">Pilih beberapa foto sekaligus. Maksimal ukuran 2MB</p>
				</div>
                <div class='input-row submit'>
                    <input type='submit' value='Next Step' class='button button-blue'/>
                </div>
                <?= form_close()?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>