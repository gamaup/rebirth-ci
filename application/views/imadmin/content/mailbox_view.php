            <div class='row'>
                <div class='col-3'>
                	<?php foreach ($data_mail as $m) { ?>
                    <div class='panel'>
                        <div class='panel-head'>
                            <h5><?= $m->subject ?></h5>
                            <div class='panel-links'>
                                <h5><?= date("d-M-Y, H:i:s", strtotime($m->date)) ?></h5>
                            </div>
                        </div>
                        <div class='panel-body'>
                            <div class='mail'>
                                <div class='head'>
                                    <div class='from'>
                                        <h4><?= $m->name ?></h4>
                                        <h5><?= $m->email ?></h5>
                                        <h5><?= $m->phone ?></h5>
                                    </div>
                                    <div class='action'>
                                        <div class='button-group'>
                                            <a href='<?= base_url() ?>imadmin/mailbox/delete/<?= $m->id ?>' class='button button-confirm'><i class='fa fa-trash-o'></i> Delete</a>
                                            <a href='<?= base_url() ?>imadmin/mailbox/' class='button'><i class='fa fa-times'></i> Close</a>
                                        </div>
                                    </div>
                                </div>
                                <p><?= $m->message ?></p>
                            </div>
                        </div>
                        <div class='panel-footer' style="text-align: right;">
                            <h5 style="display:inline-block;margin-right: 10px; line-height: 30px;">Message ID: <?= $m->id ?></h5>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>