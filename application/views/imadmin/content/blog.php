<?= $this->session->flashdata("pesan") ?>
<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Post List</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-blue datatable'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Post Date</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data_blog as $p) {
                            ?>
                            <tr>
                                <td><?= $p->id ?></td>
                                <td><?= $p->date ?></td>
                                <td><?= $p->title ?></td>
                                <td><?= $p->author ?></td>
                                <?php echo"
                                <td class='nowrap'>
                                    ".anchor(base_url()."blog/".$p->permalink, "<i class='fa fa-eye'></i>", "class='button button-icon button-blue' target='_blank'")."
                                    ".anchor(base_url()."imadmin/blog/edit/".$p->id, "<i class='fa fa-pencil'></i>", "class='button button-icon button-yellow'")."
                                    ".anchor(base_url()."imadmin/blog/cancel/".$p->id, "<i class='fa fa-trash-o'></i>", "class='button button-icon button-red button-confirm'")."
                                </td>" ?>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>