<?= $this->session->flashdata("pesan") ?>
<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-body'>
                <table class='datatable table-blue bordered'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Testimoni</th>
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data_testimoni as $p) { ?>
                        <tr>
                            <td><?= $p->id ?></td>
                            <td><?= $p->name ?></td>
                            <td><?= $p->position ?></td>
                            <td><?= $p->testimoni ?></td>
                            <?php echo"
                            <td class='nowrap'>
                                ".anchor(base_url()."imadmin/testimoni/edit/".$p->id, "<i class='fa fa-pencil'></i>", "class='button button-icon button-blue'")."
                                ".anchor(base_url()."imadmin/testimoni/delete/".$p->id, "<i class='fa fa-trash-o'></i>", "class='button button-icon button-red button-confirm'")."
                            </td>" ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>