<div class='row'>
    <?php foreach ($data_blog as $p) { ?>
    <?= form_open_multipart(base_url()."imadmin/blog/edit/".$p->id)?>
    <div class='col-1'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Post ID :</h5>
                    <input type='text' value='<?= $p->id ?>' disabled/>
                </div>
                <div class='input-row'>
                    <h5>Judul :</h5>
                    <input type='text' name='title' <?= form_error('title'); ?> value='<?= $p->title ?>'/>
                    <!-- <p class="helper">Judul Postingan</p> -->
                </div>
                <div class='input-row'>
                    <h5>Permalink :</h5>
                    <input type='text' name='permalink' <?= form_error('permalink'); ?> value='<?= $p->permalink ?>'/>
                    <!-- <p class="helper">Permalink yang digunakan untuk mengakses URL</p> -->
                </div>
                <div class='input-row file-exist'>
				    <h5>Featured Image :</h5>
				    <div class='input-file'>
                        <input type='text' name='photo-name' <?= form_error('photo-name'); ?>/>
                        <h4 class='button button-blue'>Browse</h4>
                        <input type="file" name='featured_image'/>
                        <input type='hidden' name='image_delete' value='<?= $p->featured_image ?>' />
                        <input type='hidden' name='image_delete2' value='<?= $p->featured_image_thumb ?>' />
                    </div>
                    <div class='input-file-exist'>
                        <a href='<?= base_url() ?>assets/uploads/<?= $p->featured_image ?>' target='_blank' style='background-image:url(<?= base_url() ?>assets/uploads/<?= $p->featured_image_thumb ?>);'></a>
                        <h4><i class="fa fa-retweet"></i> Change Image</h4>
                    </div>
                    <p class="helper">Minimal lebar 1360px. Maks 2MB</p>
				</div>
            </div>
        </div>
    </div>
    <div class='col-3'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
				    <h5>Isi :</h5>
				    <textarea id='wysiwyg' name='isi'><?= $p->isi ?></textarea>
				</div>
                <script>
				CKEDITOR.replace('wysiwyg');
				</script>
                <div class='input-row submit'>
                    <input type='submit' value='Update' class='button button-blue'/>
                </div>
            </div>
        </div>
    </div>
    <?= form_close()?>
    <?php } ?>
</div>
<script type="text/javascript">
function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}
$("input[name='title']").change(function(){
    var val = $(this).val();
    var slug = convertToSlug(val);
    $('input[name="permalink"]').val(slug);
});
</script>