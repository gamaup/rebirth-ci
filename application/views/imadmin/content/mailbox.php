            <div class='row'>
                <div class='col-4'>
                    <div class='panel'>
                        <div class='panel-body'>
                            <table class='mail datatable table-blue bordered'>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th class='nosort'>From</th>
                                        <th class='nosort'>Subject</th>
                                        <th class='nosort'>Read by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data_mail as $m) { ?>
                                    <tr <?php $data_con->check_read($m->id) ?>>
                                        <td><?= $m->date ?></td>
                                        <td><a href='<?= base_url() ?>imadmin/mailbox/mail/<?= $m->id ?>'><?= $m->name ?></a></td>
                                        <td><a href='<?= base_url() ?>imadmin/mailbox/mail/<?= $m->id ?>'><?= $m->subject ?></a></td>
                                        <td><?= substr(str_replace(" ",", ",$m->readby),2) ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>