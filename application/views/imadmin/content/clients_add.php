<div class='row'>
    <?= form_open_multipart(base_url()."imadmin/clients/add/")?>
    <div class='col-2'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row inline'>
                    <h5>Client Name :</h5>
                    <input type='text' name='desc' <?= form_error('desc'); ?>/>
                </div>
                <div class='input-row inline'>
				    <h5>Company Logo :</h5>
				    <div class='input-file'>
				        <input type='text'/>
				        <h4 class='button button-blue'>Browse</h4>
				        <input type="file" name='image'/>
				    </div>
                    <p class="helper">Format PNG, background transparan. Res. 200x200</p>
				</div>
                <div class="input-row inline">
                    <h5>Popularitas :</h5>
                    <select name='level'>
                        <option value='1'>Terkenal</option>
                        <option value='2'>Lumayan</option>
                        <option value='3'>Gak Terkenal</option>
                    </select>
                </div>
                <div class='input-row submit'>
                    <input type='submit' value='Save' class='button button-blue'/>
                </div>
            </div>
        </div>
    </div>
    <?= form_close()?>
</div>