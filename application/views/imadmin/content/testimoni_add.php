<div class='row'>
    <?= form_open(base_url()."imadmin/testimoni/add/")?>
    <div class='col-2'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Name :</h5>
                    <input type='text' name='name' <?= form_error('name'); ?>/>
                </div>
                <div class='input-row'>
                    <h5>Position :</h5>
                    <input type='text' name='position' <?= form_error('position'); ?>/>
                </div>
                <div class='input-row'>
				    <h5>Testimoni :</h5>
				    <textarea name='testimoni' <?= form_error('testimoni'); ?>></textarea>
				</div>
                <div class='input-row submit'>
                    <input type='submit' value='Save' class='button button-blue'/>
                </div>
            </div>
        </div>
    </div>
    <?= form_close()?>
</div>