<div class='row'>
    <div class='col-2'>
        <div class='panel'>
            <div class='steps-head steps-red'>
                <div class='step step-current'>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <div class='back-line-right'></div>
                    <span class='step-dot'></span>
                </div>
                <div class='step'>
                    <div class='back-line-left'></div>
                    <span class='step-dot'></span>
                </div>
            </div>
            <div class='panel-head'>
                <h3>Step 1: Project Info</h3>
            </div>
            <?php foreach ($data_portofolio as $p) { ?>
            <?= form_open_multipart(base_url()."imadmin/portofolio/project_info/".$p->id)?>
            <div class='panel-body'>
                <div class='input-row inline'>
                    <h5>Portofolio ID :</h5>
                    <input type='text' value='<?= $p->id ?>' disabled/>
                </div>
                <div class='input-row inline'>
                    <h5>Project Name :</h5>
                    <input type='text' name='project_name' <?= form_error('project_name'); ?> value='<?= $p->project_name ?>'/>
                    <p class="helper">Nama Project</p>
                </div>
                <div class='input-row inline'>
                    <h5>Permalink :</h5>
                    <input type='text' name='permalink' <?= form_error('permalink'); ?> value='<?= $p->permalink ?>'/>
                    <p class="helper">Permalink yang digunakan untuk mengakses URL</p>
                </div>
                <div class='input-row inline'>
                    <h5>Client :</h5>
                    <input type='text' name='client' <?= form_error('client'); ?> value='<?= $p->client ?>'/>
                    <p class="helper">Nama Klien</p>
                </div>
                <div class='input-row inline'>
                    <h5>Works :</h5>
                    <input type='text' name='works' <?= form_error('works'); ?> value='<?= $p->works ?>'/>
                    <p class="helper">Jenis project yang dikerjakan, pisahkan dengan koma</p>
                </div>
                <div class='input-row inline'>
				    <h5>Featured Image :</h5>
				    <div class='input-file'>
				        <input type='text' value='<?= $p->featured_image ?>'/>
				        <h4 class='button button-blue'>Browse</h4>
				        <input type="file" name='featured_image'/>
				    </div>
                    <p class="helper">Minimal lebar 1360px. Maksimal ukuran 2MB</p>
				</div>
                
                <div class='input-row submit'>
                    <input type='submit' value='Next Step' class='button button-blue'/>
                </div>
                <?= form_close()?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}
$("input[name='project_name']").change(function(){
    var val = $(this).val();
    var slug = convertToSlug(val);
    $('input[name="permalink"]').val(slug);
});
</script>