<?= $this->session->flashdata("pesan") ?>
<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Portofolio List</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-blue datatable'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Project Name</th>
                            <th>Client</th>
                            <th>Works</th>
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data_portofolio as $p) {
                            ?>
                            <tr>
                                <td><?= $p->id ?></td>
                                <td><?= $p->project_name ?></td>
                                <td><?= $p->client ?></td>
                                <td><?= $p->works ?></td>
                                <?php echo"
                                <td class='nowrap'>
                                    ".anchor(base_url()."project/".$p->permalink, "<i class='fa fa-eye'></i>", "class='button button-icon button-blue' target='_blank'")."
                                    ".anchor(base_url()."imadmin/portofolio/cancel/".$p->id, "<i class='fa fa-trash-o'></i>", "class='button button-icon button-red button-confirm'")."
                                </td>" ?>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>