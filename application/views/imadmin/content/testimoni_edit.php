<div class='row'>
    <?php foreach ($data_testimoni as $p) { ?>
    <?= form_open_multipart(base_url()."imadmin/testimoni/edit/".$p->id)?>
    <div class='col-2'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Testimoni ID :</h5>
                    <input type='text' value='<?= $p->id ?>' disabled/>
                </div>
                <div class='input-row'>
                    <h5>Name :</h5>
                    <input type='text' name='name' <?= form_error('name'); ?> value='<?= $p->name ?>'/>
                </div>
                <div class='input-row'>
                    <h5>Position :</h5>
                    <input type='text' name='position' <?= form_error('position'); ?> value='<?= $p->position ?>'/>
                </div>
                <div class='input-row'>
				    <h5>Testimoni :</h5>
				    <textarea name='testimoni' <?= form_error('testimoni'); ?>><?= $p->testimoni ?></textarea>
				</div>
                <div class='input-row submit'>
                    <input type='submit' value='Update' class='button button-blue'/>
                </div>
            </div>
        </div>
    </div>
    <?= form_close()?>
    <?php } ?>
</div>