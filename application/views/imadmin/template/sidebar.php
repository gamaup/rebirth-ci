<aside>
            <div class='profil'>
                <div class='foto' style='background-image:url(<?=base_url()?>assets/images/pp.jpg);'>
                </div>
                <div class='info'>
                    <?php $data_login = $this->session->userdata('logged_in') ?>
                    <h4><?= $data_login['display_name'] ?></h4>
                    <h5><?= $data_login['role'] ?></h5>
                </div>
                <div class='opsi'>
                    <div class='button-group'>
                        <i class="fa fa-cog dropdown-toggle"></i>
                        <ul class='dropdown-menu right-pos'>
                            <li class='head'>Profile Option</li>
                            <li><a href='<?= base_url() ?>imadmin/manage_user/edit/<?= $data_login['username'] ?>'>Settings</a></li>
                            <li class='sparator'></li>
                            <li><a href='<?= base_url() ?>imadmin/login/logout'>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class='nav outer'>
                <li <?= $pos_parent == 'dashboard' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin'><i class="fa fa-tachometer"></i>Dashboard</a></li>
                <li <?= $pos_parent == 'mailbox' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/mailbox'><i class="fa fa-envelope"></i>Mailbox</a></li>
                <li class='second <?= $pos_parent == 'blog' ? "active" : ""; ?>'><a><i class="fa fa-pencil"></i>Blog<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'blog' && $pos_child == 'postlist' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/blog'>Posts</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'blog' && $pos_child == 'new_post' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/blog/add'>New Post</a></li>
                    </ul>
                </li>
                <li class='second <?= $pos_parent == 'portofolio' ? "active" : ""; ?>'><a><i class="fa fa-archive"></i>Portofolio<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'portofolio' && $pos_child == 'portofoliolist' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/portofolio'>Portofolio List</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'portofolio' && $pos_child == 'new_portofolio' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/portofolio/add'>New Portofolio</a></li>
                    </ul>
                </li>
                <li <?= $pos_parent == 'testimoni' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/testimoni'><i class="fa fa-quote-right"></i>Testimoni</a></li>
                <li <?= $pos_parent == 'clients' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/clients'><i class="fa fa-briefcase"></i>Clients</a></li>
                <?php if ($data_login['access_level'] <= 1) { ?>
                <li class='second <?= $pos_parent == 'manage_user' ? "active" : ""; ?>'><a><i class="fa fa-users"></i>Manage User<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'manage_user' && $pos_child == 'user_list' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/manage_user'>User List</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'manage_user' && $pos_child == 'add_user' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/manage_user/add'>Add User</a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </aside>