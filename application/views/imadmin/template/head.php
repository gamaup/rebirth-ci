<title><?= $pagetitle; ?></title>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/plugins/datatables/css/jquery.dataTables.css'/>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/imadmin.css'/>
    <script src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= base_url() ?>assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/js/imadmin.js"></script>
    <script src="<?= base_url() ?>assets/plugins/cloudinary/jquery.cloudinary.js"></script>
    <script src="<?= base_url() ?>assets/plugins/cloudinary/jquery.fileupload.js"></script>
    <script src="<?= base_url() ?>assets/plugins/cloudinary/jquery.iframe-transport.js"></script>
    <script src="<?= base_url() ?>assets/plugins/cloudinary/jquery.ui.widget.js"></script>
    <script type="text/javascript">$.cloudinary.config({ cloud_name: 'inncomedia', api_key: '695457798571332'})</script>