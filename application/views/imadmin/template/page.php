<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('imadmin/template/head'); ?>
</head>
<body>
    <div id='wrapper'>
        <?php $this->load->view('imadmin/template/topnav'); ?>
        <?php $this->load->view('imadmin/template/sidebar'); ?>
        <main>
            <?php $this->load->view('imadmin/template/header'); ?>
            <?php
            if (isset($content)) {
                $this->load->view($content);
            }
            ?>
        </main>
    </div>
</body>
</html>