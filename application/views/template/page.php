<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('template/head'); ?>
    </head>
    <body>
        <?php $this->load->view('template/nav'); ?>
        <main>
            <?php $this->load->view($content); ?>
            <?php $this->load->view('template/footer'); ?>
        </main>
    </body>
</html>