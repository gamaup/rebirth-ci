<title><?= $title ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
<?php
if (isset($plugins_css)) {
    foreach ($plugins_css as $css) {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().$css.'"/>
        ';
    }
}
?>
<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/style.css'/>
<script src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<?php
if (isset($plugins_js)) {
    foreach ($plugins_js as $js) {
        echo '<script src="'.base_url().$js.'"></script>
        ';
    }
}
?>
<script src="<?= base_url() ?>assets/js/script.js"></script>
<link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/images/favicon.png">
<?php
if (isset($meta)) {
    foreach ($meta as $prop => $cont) {
        echo '<meta property="'.$prop.'" content="'.$cont.'"/>
        ';
    }
}
?>