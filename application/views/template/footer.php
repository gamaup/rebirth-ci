<footer>
    <div class='container'>
        <img class='footer-logo' src='<?= base_url() ?>assets/images/footer-logo.png'/>
        <h4 class='keepintouch'>Keep In Touch : <a href='https://www.facebook.com/inncomedia' target='_blank'><i class='fa fa-facebook'></i></a><a href='https://twitter.com/inncomedia' target='_blank'><i class='fa fa-twitter'></i></a><a href='https://www.youtube.com/channel/UCQJz7FEMlGULWDEJz8eeZvw' target='_blank'><i class='fa fa-youtube'></i></a></h4>
        <div class='copyright'>
            <p>Inncomedia</p>
            <p>&copy; 2013-<?= date('Y'); ?></p>
        </div>
    </div>
    <div class='backtotop'>
        <i class="fa fa-caret-up"></i>
    </div>
</footer>