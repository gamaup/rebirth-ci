<nav>
    <div class='container'>
        <img src='<?= base_url() ?>assets/images/logo-inncomedia.png' class='nav-logo'/>
        <ul class='nav-group'>
            <li <?= $pos == 'home' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>'>Home</a></li>
            <li <?= $pos == 'about' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>about'>About Us</a></li>
            <li <?= $pos == 'whatwedo' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>whatwedo'>What We Do</a></li>
            <li <?= $pos == 'portofolio' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>portofolio'>Portofolio</a></li>
            <li <?= $pos == 'product' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>product'>Product</a></li>
            <li <?= $pos == 'clients' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>clients'>Clients</a></li>
            <li <?= $pos == 'blog' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>blog'>Blog</a></li>
            <li <?= $pos == 'contact' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>contact'>Contact</a></li>
        </ul>
        <div class='push-nav-toggle'>
            <span></span>
        </div>
    </div>
</nav>
<div class='push-nav'>
    <ul class='push-nav-group'>
        <li <?= $pos == 'home' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>'>Home</a></li>
        <li <?= $pos == 'about' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>about'>About Us</a></li>
        <li <?= $pos == 'whatwedo' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>whatwedo'>What We Do</a></li>
        <li <?= $pos == 'portofolio' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>portofolio'>Portofolio</a></li>
        <li <?= $pos == 'product' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>product'>Product</a></li>
        <li <?= $pos == 'clients' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>clients'>Clients</a></li>
        <li <?= $pos == 'blog' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>blog'>Blog</a></li>
        <li <?= $pos == 'contact' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>contact'>Contact</a></li>
    </ul>
</div>