<!DOCTYPE html>
<html>
<head>
	<title>404 Page Not Found</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
</head>
<body>
	<style type="text/css">
		img {
			display: block;
			margin: 80px auto 40px;
			padding: 0 20px;
			max-width: 400px;
			width: 100%;
		}
		p {
			text-align: center;
			margin-bottom: 20px;
		}
		p a {
			text-decoration: none;
			display: inline-block;
			margin: 5px 10px;
			padding: 10px 15px;
			color: #fff;
			font-family: 'OpenSans-Light';
			background-color: #35a1ab;
			cursor: pointer;
		}
	</style>
	<img src='<?= base_url('assets/images/404.png') ?>'/>
	<p>Ouch! Sepertinya Anda tersesat.</p>
	<p><a onclick="history.go(-1);">Back to Previous Page</a><a href='<?= base_url() ?>'>Back to Homepage</a></p>
</body>
</html>