<?php
date_default_timezone_set("Asia/Bangkok");

Class User_model extends CI_Model {

	function login($username, $password) {
		$this->db->select('*');
        $this->db->from('user');
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
	}

	function update_last_login($username) {
		$date = date('Y-m-d H:i:s');
		$data = array('last_login' => $date);
		$this->db->where('username', $username);
        $this->db->update('user', $data);
	}

	function get_all_user() {
		$q = $this->db->get('user');
		return $q->result();
	}

	function get_user_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('user');
		return $q->result();
	}

	function get_user_by_username($username) {
		$this->db->where('username', $username);
		$q = $this->db->get('user');
		return $q->result();
	}

	function add_user($data) {
        $this->db->insert('user', $data); 
    }

	function edit_user($data, $username) {
		$this->db->where("username", $username);
        $this->db->update('user', $data);
    }

    function delete_user($username) {
        $this->db->where('username', $username);
        $this->db->delete('user'); 
    }

    function get_all_role() {
    	$q = $this->db->get('user_role');
		return $q->result();
    }

	function get_role_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('user_role');
		$ret = $q->row();
		return $ret->role;
	}

	function get_access_level_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('user_role');
		$ret = $q->row();
		return $ret->access_level;
	}
}