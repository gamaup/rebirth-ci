<?php
date_default_timezone_set("Asia/Bangkok");

Class Mailbox_model extends CI_Model {

	function get_all_mail() {
		$q = $this->db->get('mailbox');
		return $q->result();
	}

	function get_mail_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('mailbox');
		return $q->result();
	}

    function delete_mail($id) {
        $this->db->where('id', $id);
        $this->db->delete('mailbox'); 
    }

	function get_readby_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('mailbox');
		$ret = $q->row();
		return $ret->readby;
	}

	function update_readby($id, $data) {
		$this->db->where("id", $id);
        $this->db->update('mailbox', $data);
    }

    function send($data) {
		$this->db->insert('mailbox', $data);
    }
}