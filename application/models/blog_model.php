<?php
date_default_timezone_set("Asia/Bangkok");

Class Blog_model extends CI_Model {

	function get_all_blog() {
		$this->db->where('status', '1');
		$q = $this->db->get('blog');
		return $q->result();
	}

	function clean_blog() {
		$this->db->delete('blog', array('status' => '0')); 
	}

	function delete($id) {
		$this->db->delete('blog', array('id' => $id)); 
	}

	function create_new() {
		$data = array('status' => '0');
		$this->db->insert('blog', $data);
		return $this->db->insert_id();
	}

	function get_blog_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('blog');
		return $q->result();
	}

	function update_blog($data, $id) {
		$this->db->where("id", $id);
        $this->db->update('blog', $data);
    }

	function get_blog_by_permalink($permalink) {
		$this->db->where('permalink', $permalink);
		$q = $this->db->get('blog');
		return $q->result();
	}

	function check_blog_by_permalink($permalink) {
		$this->db->where('permalink', $permalink);
		$q = $this->db->get('blog');
		return $q->num_rows();
	}

	function get_recent_blog() {
		$this->db->where('status', '1');
		$this->db->order_by('date', 'desc');
		$this->db->limit(4);
		$q = $this->db->get('blog');
		return $q->result();
	}
}