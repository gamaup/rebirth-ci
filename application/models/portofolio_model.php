<?php
date_default_timezone_set("Asia/Bangkok");

Class Portofolio_model extends CI_Model {

	function get_all_portofolio() {
		$this->db->where('status', '1');
		$this->db->order_by('id', 'desc');
		$q = $this->db->get('portofolio');
		return $q->result();
	}

	function get_recent_portofolio() {
		$this->db->where('status', '1');
		$this->db->order_by('id', 'desc');
		$this->db->limit(4);
		$q = $this->db->get('portofolio');
		return $q->result();
	}

	function clean_portofolio() {
		$this->db->delete('portofolio', array('status' => '0')); 
	}

	function delete($id) {
		$this->db->delete('portofolio', array('id' => $id)); 
		$this->db->delete('portofolio_gallery', array('portofolio_id' => $id)); 
	}

	function create_new() {
		$data = array('status' => '0');
		$this->db->insert('portofolio', $data);
		return $this->db->insert_id();
	}

	function get_portofolio_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('portofolio');
		return $q->result();
	}

	function update_portofolio($data, $id) {
		$this->db->where("id", $id);
        $this->db->update('portofolio', $data);
    }

    function upload_gallery($data) {
    	$this->db->insert_batch('portofolio_gallery',$data);
    }

	function get_id_by_permalink($permalink) {
		$this->db->where('permalink', $permalink);
		$q = $this->db->get('portofolio');
		$ret = $q->row();
		return $ret->id;
	}

	function check_portofolio_by_permalink($permalink) {
		$this->db->where('permalink', $permalink);
		$q = $this->db->get('portofolio');
		return $q->num_rows();
	}

	function get_portofolio_gallery_by_id($id) {
		$this->db->where('portofolio_id', $id);
		$q = $this->db->get('portofolio_gallery');
		return $q->result();
	}

	function get_portofolio_by_permalink($permalink) {
		$this->db->where('permalink', $permalink);
		$q = $this->db->get('portofolio');
		return $q->result();
	}
}