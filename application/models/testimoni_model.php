<?php
date_default_timezone_set("Asia/Bangkok");

Class Testimoni_model extends CI_Model {

	function get_all_testimoni() {
		$q = $this->db->get('testimoni');
		return $q->result();
	}

	function get_selected_testimoni() {
		$this->db->where('status', '1');
		$q = $this->db->get('testimoni');
		return $q->result();
	}

	function delete($id) {
		$this->db->delete('testimoni', array('id' => $id));
	}

	function add_testimoni($data) {
		$this->db->insert('testimoni', $data);
	}

	function get_testimoni_by_id($id) {
		$this->db->where('id', $id);
		$q = $this->db->get('testimoni');
		return $q->result();
	}

	function update_testimoni($data, $id) {
		$this->db->where("id", $id);
        $this->db->update('testimoni', $data);
    }
}